---
title: "Sur cette terre comme au ciel"
date: 2021-11-04T20:13:42+01:00
draft: false
genres: 
- Roman
- Aventure
author: "Davide Enia"
fiction: true
release_year: "2012"
duree:
# maj: "08 Fév 2021"
#cover: 
  img: /covers/DerekSivers-AnythingYouWant-cover.jpg
  source: https://sive.rs/a/
#toc: false
---

## 🗣 De quoi ça parle

Le roman retrace la vie d'un jeune Palermitain dans les années 50. Grâce à son oncle lui-même ancien boxeur vice-champion d'Italie, il découvre la boxe après un combat de rue contre ses camarades. Son oncle devient en quelque sorte son mentor, celui qui lui apprend la vie à sa manière et qui lui raconte ses anecdotes de vécu. On suit donc les aventures du jeune Davidù lors de ses conquêtes amoureuses, de ses combats de boxe et de ses moments passés avec sa famille. Son but est de conquérir le titre de champion national de boxe, là où son oncle et son grand-père ont échoué. Il cherche aussi à conquérir le cœur d'une fille de son âge dont il est amoureux depuis son premier combat de rue. 

## 🔎 Comment je l'ai découvert

Ma mère me l'a conseillé

## 💭 Ce que j'en ai pensé

J'ai beaucoup aimé l'histoire et l'atmosphère qui s'en dégage. Davidù est un garçon espiègle et téméraire qui met toute son énergie dans sa passion pour la boxe. On se retrouve happé par le récit et mêlé au destin de notre protagoniste avec une pointe de suspense quant à l'issue des combats de boxe. Les nombreux retours en arrière avec des changements de narrateur troublent parfois la compréhension mais le livre reste facile à lire dans son ensemble. 

## 🎯 Qui l'apprécierait

Ceux qui aiment la boxe, ceux qui aiment les histoires d'enfance où l'on suit la vie quotidienne du personnage principal avec ses émotions, ses envies, ses hésitations et que l'on voit grandir au fur et à mesure.   

## 📚 Livres reliés

Les romans sur l'Italie des années 50 notamment sur le sud de l'Italie et la Sicile. Les livres parlant de boxe. Raging Bull de Martin Scorsese.

