---
title: "Le comte de Monte Cristo"
date: 2021-11-22T01:08:09+01:00
draft: false 
genres: 
- Roman
- Aventure
- Histoire
author: "Alexandre Dumas"
fiction: true
release_year: "1844-1846"
duree:
# maj: "08 Fév 2021"
#cover: 
  img: 
  source: 
#toc: false
---

## 🗣 De quoi ça parle

Edmond Dantès est un jeune marin marseillais embarqué sur un navire marchand. De retour à Marseille, il est sur le point d'épouser sa fiancée Mercédès, qui est également courtisée par un autre jeune homme. Le capitaine étant mort durant le dernier voyage, c'est à Edmond que revient l'honneur d'occuper sa place. Malheureusement, le comptable du navire ne peut supporter l'idée de voir Dantès prendre ce poste. Il invente alors une dénonciation machiavélique colportée par le courtisan de Mercédès qui va bouleverser la vie d'Edmond Dantès à jamais.

## 🔎 Comment je l'ai découvert

Qui ne connaît pas les œuvres ultra célèbres d'Alexandre Dumas ?

## 💭 Ce que j'en ai pensé

C'est sans aucun doute mon roman préféré de l'année voire des 5 dernières. C'est un pur chef-d'œuvre. Malgré ses 2000 pages, le livre se lit très facilement. L'histoire est touchante, prenante et émouvante. On se glisse dans la peau d'Edmond Dantès alias le comte de Monte Cristo et on admire son intelligence et son discernement. Le caractère du personnage évolue sensiblement au fil des tomes car les années passent et les épreuves que traversent le héros l'éprouvent durement. Si au premier tome, l'histoire est vue à travers les yeux d'Edmond avec son ressenti et ses émotions, les suivants sont beaucoup plus distants et ne parviennent pas à sonder l'âme du comte. C'est comme si l'on avait deux personnages complètement différents. Alors que le premier était simple, heureux et attachant, le deuxième est froid, cynique, laconique et impénétrable. Pourtant on éprouve quand-même de l'empathie pour ce dernier. Cela s'explique par l'injustice qu'il a subie et on se plaît à voir s'opérer sa terrible vengeance mûrement réfléchie et si finement amenée. 

L'histoire est extrêmement bien construite du début à la fin avec une imbrication des évènements et des personnages tout simplement superbes. La longueur du livre permet à l'auteur de développer ses personnages jusqu'à un point rarement atteint auparavant et le dénouement tant attendu devient alors magistral. Chacun de ces personnages est amené à se retrouver sur le chemin de Monte Cristo et ils sont souvent liés les uns aux autres ce qui rend le récit encore plus palpitant. On admire et on plaint la souffrance endurée par Edmond qui est tombé si bas. Puis, son retour plus impressionnant que jamais dévoile toute la grandeur de ce héros mythique. N'attendez plus pour vous lancer dans l'aventure du comte de Monte Cristo !

## 🎯 Qui l'apprécierait

Les amateurs de Dumas et de romans sur le XIXe siècle avec de nombreux détails historiques.

## 📚 Livres reliés

Les autres livres de l'auteur comme les Trois Mousquetaires.