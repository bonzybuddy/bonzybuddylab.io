---
title: "Voyage au bout de la nuit"
date: 2022-03-05T10:48:15+01:00
draft: False
genres: 
- Histoire
- Roman
author: "Louis-Ferdinand Céline"
fiction: true
release_year: "1932"
duree:
# maj: "08 Fév 2021"
#cover: 
  img: /covers/DerekSivers-AnythingYouWant-cover.jpg
  source: https://sive.rs/a/
#toc: false
---
## 🗣 De quoi ça parle

L'histoire raconte la vie de Ferdinand Bardamu entre la Première Guerre Mondiale et l'entre deux-guerres. Le personnage de Bardamu est fortement calqué sur l'auteur lui-même. On suit Ferdinand à travers différentes péripéties entre la France, l’Afrique et les Etats-Unis. Plusieurs personnages sont récurrents et suivent le même parcours que le protagoniste, à l’instar de Robinson. Néanmoins Ferdinand échoue souvent et a du mal à se faire accepter et r

## 🔎 Comment je l'ai découvert

C'est un livre très connu, classique de la littérature contemporaine française que j'ai trouvé dans la bibliothèque de ma mère.

## 💭 Ce que j'en ai pensé

Le livre est assez long mais il s'y déroule beaucoup d'événements. La manière d'écrire de Céline est novatrice car il utilise le langage parlé, assez rude, empreint d'argot du début du XXe siècle ce qui rend la lecture assez difficile. J'ai souvent dû relire certaines phrases pour les comprendre. Le vocabulaire est lui aussi bien fourni avec des mots presque disparus aujourd'hui.

S'il est considéré comme une oeuvre majeure du début du siècle dernier, j’ai personnellement peu accroché au récit, mais je vous conseille de vous faire votre propre avis en le lisant 😉

## 🎯 Qui l'apprécierait

Ceux qui aiment les longs récits, les épopées.

## 📚 Livres reliés

Le deuxième livre de Céline intitulé "Mort à Crédit" qui est la suite de "Voyage au bout de la nuit".