---
title: "Le plâtrier siffleur"
date: 2022-03-07T11:05:21+01:00
draft: false
genres: 
- Poésie
author: "Christian Bobin"
fiction: true
release_year: "2018"
duree:
# maj: "08 Fév 2021"
#cover: 
  img: /covers/DerekSivers-AnythingYouWant-cover.jpg
  source: https://sive.rs/a/
#toc: false
---

## 🗣 De quoi ça parle

Trop court pour résumer ! 

En une quinzaine de pages l’auteur nous partage son point de vue sur la vie et le monde qui l’entoure.

## 🔎 Comment je l'ai découvert

Ma mère me l’a offert pour mon anniversaire.

## 💭 Ce que j'en ai pensé

C’est une oeuvre très subjective où certains passages vous étonneront sûrement.

## 🎯 Qui l'apprécierait

Les amateurs de poésie.