---
title: "Make Time"
date: 2021-03-15T07:30:43+01:00
# maj: "08-02-2021"
draft: false
author: John Zeratsky & Jake Knapp
fiction: false
genres:
  - Life Advice
  - Productivité
release_year: "2018"
cover:
  img: https://maketime.blog/wp-content/uploads/2019/03/crop-0-0-1000-1344-0-Jake-Cover-3D-new-762x1024.jpg
  source: https://maketime.blog/
---

## 🥇🥈🥉 Le livre en 3 phrases

- Se créer du temps libre (sans méthode miracle)
- Ralentir son mode de vie pour se focaliser sur soi-même et ses proches
- Adopter des habitudes très simples mais efficaces pour améliorer sa qualité de vie

## 🎣 Impressions

Au commencement, je me suis dit "Oh, encore un livre sur comment gérer son temps bla bla bla..." puis j'ai mieux cerné le but des auteurs et je me suis reconnu dans plusieurs situations. J'ai souvent apprécié leur manière de penser car elle correspond globalement à la mienne. Le livre reprend pas mal de concepts ou idées d'autres livres comme [Getting Things Done]({{< ref "../books/getting_things_done/" >}}), [Atomic Habits]({{< ref "../books/atomic_habits/" >}}), [The 4-Hour Workweek]({{< ref "../books/the_4_hour_workweek/" >}}), Deep Work, les livres de Marie Kondo et bien d'autres. Le livre prône des choses simples qui sont expliquées en quelques pages à chaque fois. Vous pouvez donc facilement sauter plusieurs méthodes si vous les connaissez et ne sélectionner que celles qui vous plaisent. Le tout est très facile à lire et à comprendre. Je vous encourage à suivre leurs conseils 😊 Une belle découverte donc !

## 🔎 Comment je l'ai découvert

Une des recommandations d'Ali Abdaal

## 🎯 Qui devrait le lire

Ceux qui sont à la recherche de plus de temps pour eux ou pour des projets personnels mais qui ne savent pas comment s'y prendre. Ce qui veulent ralentir la cadence (infernale) dans laquelle ils se trouvent. Ceux qui souhaitent se libérer de l'aliénation technologique liée aux smartphones et Internet. Le livre peut aussi être une bonne recommandation pour quelqu'un qui ne se rend pas compte de tout le temps qu'il perd.

## 🎉 Ce que le livre a changé chez moi

**Comment ma vie / comportement / idées / pensées ont évolués après la lecture de ce livre.**

- J'appliquais déjà une bonne partie des méthodes proposées dans le livre avant de le lire. Néanmoins sur les 87 (!) j'en ai (re)découvert certaines comme par exemple la sieste qui permet de recharger ses batteries dans la journée.
- Cela me conforte dans ma manière de penser sur l'utilisation des nouvelles technologies et sur l'amélioration de mon mode de vie.
- Sincèrement, je pense qu'une bonne partie de la solution réside dans le fait d'être conscient de perdre du temps inutilement et d'être convaincu par un changement de mode de "consommation". Si vous n'êtes pas convaincu par tout ce qui suit, il vous sera difficile d'appliquer ces conseils sur le long terme.

## 💬 Mes 3 citations préférées

> The best tactics are the ones that fit into your day.

> Sometimes we don't know what we're capable of until we apply some simple tactics and an experimental mindset to our lives.

> We're built for one world, but we live in another.

## 📝 Résumé + notes personnelles

Le livre repose sur 4 parties (Highlight, Laser, Energize, Reflect) qui décrivent le mode d'emploi de ce que préconisent les auteurs (anciens designers produit chez Google pour Gmail et YouTube). Ces parties sont dépendantes et forment un tout. Chacune contient plusieurs "tactiques" qui sont en fait des conseils. Il est précisé que chaque conseil peut (et doit) être adapté individuellement mais qu'ils ne sont pas TOUS à suivre. Choisissez, essayez, ajustez. Ces conseils sont au nombre de 87 donc je ne vais pas revenir sur tous mais faire une sélection de ceux que je trouve les plus pertinents et intéressants. Si vous voulez en savoir plus je vous invite à lire le livre ou à consulter le [site web](https://maketime.blog/).

Commençons d'abord par un état des lieux de notre mode de vie moderne. La plupart d'entre nous sommes attachés à nos téléphones toute la journée. Nous vivons au gré des notifications, mails et autres réseaux sociaux. Jusqu'ici rien de nouveau mais que vous en soyez conscient ou pas, nous passons énormément de temps à ne rien faire de "productif" et à être distrait. Sans vouloir rejeter la faute sur qui que ce soit, il se trouve que c'est le mode de fonctionnement "par défaut" aujourd'hui. Être joignable partout et tout le temps, faire toujours plus de choses, faire passer les priorités des autres avant les nôtres etc. bref, autant de choses dont vous avez probablement déjà entendu parler. Mais ce sujet me tient à cœur. Plutôt que de se lamenter et chercher un coupable, je (et les auteurs du livre) proposent des solutions pour changer ce mode "par défaut". Ils définissent donc deux principales distractions : _the Busy Bandwagon_ représentant notre culture qui veut que nous soyons constamment occupés, incités à faire toujours plus et _the Infinity Pools_ qui représente les applications ou services qui ont un contenu infini via un _pull to refresh._

### Highlight

L'objectif très simple de cette première partie est de définir chaque jour un unique "but" que l'on s'efforcera d'accomplir. Ce but doit être unique et facilement réalisable. Pour nous aider à choisir, les auteurs proposent 3 sous-catégories :

- **Urgence** une chose importante et urgente à finir pour aujourd'hui
- **Satisfaction** quelque chose qui vous rend satisfait et soulagé de l'avoir fait comme écrire cette review
- **Joie** profiter de la vie et d'un moment de plaisir comme passer du temps avec ses amis ou ses enfants, lire un livre etc.

Je trouve que la frontière entre les 2 dernières est faible mais libre à vous de choisir n'importe quelle autre catégorie qui vous convient. En fonction de ces critères, vous choisissez quel sera votre accomplissement principal de la journée. Cette tactique permet de se sentir satisfait d'avoir réalisé ce que l'on a défini comme LE but de notre journée. Nous serions plus heureux et satisfait de notre journée en accomplissant une tâche d'ampleur moyenne plutôt que plusieurs petites mais qui nous incitent à en faire toujours plus. Votre _Highlight_ doit donc être au sommet de vos priorités de la journée. Adaptez celle-ci, bloquez un créneau dans votre calendrier. J'ai adopté cette tactique pendant plusieurs mois mais j'ai finalement arrêté car je trouve qu'elle ne me correspond pas pour l'instant. Mais à vous peut-être !

### Laser

Ici l'objectif est d'être focus sur son _Highlight_ et son travail en général. Notre monde "moderne" contient trop de distractions qui nous empêchent de travailler correctement et nous font perdre du temps. Mais en réalité nous avons juste recréé des instincts naturels via tous ces services. C'est ce que je souligne dans la review d'_Atomic Habits._ C'est le moment de parler des _Infinity Pools._ YouTube, Facebook, Twitter, Netflix, la TV, vos mails font partie de ces _Infinity Pools._ A chaque fois que vous rafraîchissez la page, du nouveau contenu apparaît. Vous êtes alors tentés de regarder toujours plus. Pour éviter cela, il faut revoir la manière dont vous utilisez votre téléphone. Et ne comptez pas sur les entreprises ou les gouvernements pour faire ce travail à votre place. Prenez vous-mêmes conscience de l'ampleur de la chose et réagissez. Attention à ne pas mal interpréter cela, je (et les auteurs) ne prônent pas la suppression totale et radicale des nouvelles technologies, ce serait insensé. Utilisez-les en connaissance de causes et sans abus.

- Désinstaller purement et simplement les applications fautives. Cela inclut Facebook, Instagram, Snapchat, Twitter, YouTube, TikTok, Gmail et j'en passe. Le pas peut sembler dur à franchir mais il en vaut la peine ! Vous pouvez toujours les réinstaller rapidement. Hormis mes mails et YouTube, c'est une étape que j'ai réalisé.
- Déconnectez-vous des sites pour qu'il soit plus long et réticent de vous y reconnecter
- Désactivez les notifications pour ne plus être dérangé. Soyons honnêtes, vous recevez rarement une notification **réellement** importante. Le mode "Ne pas déranger" peut être intéressant car il coupe toutes les notifications d'un coup. Sinon vous pouvez aussi paramétrer chaque application.
- Portez une montre pour éviter de regarder l'heure sur son téléphone. Si comme moi, vous aimez les montres et la technicité qui la compose cela fera une opportunité de vous y intéresser davantage. Sinon il existe des montres très simples de tous les prix et elles donnent parfaitement l'heure donc n'hésitez pas.
- (Pas dans le livre mais que j'applique moi-même) Désactivez l'allumage automatique de l'écran quand vous recevez une notification. En prime, cela préserve votre batterie 😉
- Éloignez vos téléphones et ordinateurs notamment de votre chambre le soir. Cela vous permettra de lire un livre ou de passer du temps avec votre famille. J'ai pris l'habitude d'éteindre mon téléphone vers 21-22h et de lire avant de me coucher. C'est beaucoup plus apaisant que de scroller sur Instagram. Si j'ai l'occasion je regarde un film.
- Ne regardez pas votre téléphone dès que vous vous levez le matin. Pourquoi s'infliger tout de suite le stress des messages, des mails ou des notifications ?
- Limitez ou supprimez les infos. J'ai franchi le pas depuis longtemps, s'il y a une vraie info importante, j'en entendrai forcément parler (cas du Covid par exemple).
- Oubliez la TV. Je n'ai plus de télé depuis que je vis seul et je n'en ai jamais ressenti l'utilité alors que je la regardais souvent quand j'étais petit. J'espère que franchir le cap sera aussi facile pour vous.
- N'accordez que peu de temps aux mails. Deux fois par jour devrait suffire [(The 4-Hour Workweek)]({{< ref "../books/the_4_hour_workweek/" >}}). Videz votre mail boîte régulièrement comme proposé dans [Getting Things Done]({{< ref "../books/getting_things_done/" >}}). Je ne reçois pas beaucoup de mails donc même si je vérifie souvent, je ne perds pas beaucoup de temps à le faire mais je pense que cette technique est très importante pour ceux qui reçoivent un nombre important de mails.

Voilà un (petit) aperçu de tout ce que vous pouvez faire pour rester focus sur votre travail.

### Energize

On sépare souvent le cerveau du reste du corps. Mais pour fonctionner correctement, le cerveau a besoin de plusieurs paramètres. Dans cette partie du livre, les auteurs s'attardent sur un point qui me touche particulièrement depuis quelques mois. La (re)découverte du mode de vie de l'homme des cavernes, le fameux chasseur-cueilleur dont je parle dans mon article sur la nutrition. Mais je vous vois déjà vous moquer et dire que je suis fou. Je n'ai pas dit qu'il fallait revenir 20 000 ans en arrière mais seulement faire correspondre ce dont notre corps a besoin avec un environnement moderne comme le notre. Nous avons peu évolué génétiquement comparé aux gigantesques avancées technologiques survenues depuis le Paléolithique. C'est la raison pour laquelle nous sommes encore très proches de nos ancêtres _Homo Sapiens_ et que nous ne sommes pas vraiment adapté au monde moderne dans lequel nous vivons aujourd'hui. Encore une fois, l'idée n'est pas de revenir à une civilisation arriérée dépourvue de technologie et de médecine. Si vous aimez l'idée voici quelques conseils pour faire ressortir votre côté paléo...

- Bougez ! Marchez, prenez les escaliers, courrez, dépensez-vous ne serait-ce que 10-20mn par jour. L'important est de le faire tous les jours. Étant un sportif assidu, je considérais que les petites activités ne valaient rien ; seule une vraie bonne séance de sport était efficace. Je me trompais, pour être en bonne forme nul besoin de faire du triathlon, il suffit simplement de faire une activité physique modérée chaque jour. Rien n'empêche de s'entraîner de manière plus intense durant une période plus longue de temps libre.
- Mangez correctement. N'achetez pas de produits ultra transformés qui ne conviennent pas du tout à notre corps et donc au bon fonctionnement de notre cerveau. Mangez des légumes, manger sainement. La question du jeûne (intermittent) mérite d'être posée et était le quotidien de l'_Homo Sapiens._ Commencez par tester les effets sur votre corps et avancez par palier pour éviter tout déséquilibre brutal.

> L’alimentation est la première médecine. Il n’y a que nos sociétés occidentales qui font mine de l’ignorer. Scott Jurek

Pour plus de détails et ne pas me répéter je vous invite à lire [mon article sur le sujet]({{< ref "../articles/alim/" >}}).

- Sortez, prenez un bon bol d'air pour vous ressourcer, la forêt est une très bonne option. A défaut allez dans un parc. Lorsque vous sortez ne prenez pas toujours vos écouteurs, appréciez les bruits de dehors (pas les klaxons ou le bruit du métro bien sûr !). Prenez des vraies journées de pause. Entrainez-vous à méditer. Je me suis lancé dans ce dernier point depuis quelques semaines dans l'optique de me focaliser sur moi-même et de tester une nouvelle pratique. Je ne peux que vous le conseiller ! Je n'utilise pas d'application pour me guider car je veux juste être au calme et écouter mon corps mais si vous ressentez le besoin d'être accompagné, certaines appli sont très bien faites.
- Passez du temps avec vos amis et votre famille. L'interaction sociale était au cœur des préoccupations de l'_Homo Sapiens._ Aujourd'hui Facebook et Instagram ont supplanté la rencontre physique. Si vous avez la chance de manger entre amis ou avec votre famille, profitez de cet instant pour discuter avec eux et laisser les écrans loin de vous, téléphones ou TV n'ont pas besoin de se retrouver à table.
- Aménager votre chambre pour qu'elle soit uniquement un lieu de "sommeil". Pas de télé, pas d'ordinateur. Posez un livre sur votre table de chevet ou sous l'oreiller comme moi ^^ Évidemment si vous habitez dans un studio, vous n'avez qu'une seule pièce où tout est mélangé mais essayez de faire la part des choses en vous allouant du temps pour chaque moment de la journée et gardez intact le moment au lit. Faites aussi attention aux lumières que vous utilisez dans vos différentes pièces. Je reviendrai sur le sujet de la lumière dans un prochain article.
- Vous pouvez tester les siestes. Je m'y étais mis durant une période mais je ne pouvais pas en faire à l'école ce qui m'a contraint à arrêter. Malgré ça, j'ai pu constater qu'il était bien plus simple que prévu de s'assoupir 15-20mn après le déjeuner ou dans l'après-midi et que l'effet de bien-être se faisait ressentir immédiatement. Ensuite c'est à vous de trouver vos cycles de sommeil, vos horaires de fatigue pour déterminer si faire des siestes dans la journée vous convient ou pas.

Avec ces quelques clés, vous parviendrez à vous sentir mieux dans votre corps ce qui est indispensable pour mieux réfléchir avec son cerveau.

### Reflect

Cette dernière partie sera courte. Notez vos résultats au fur et à mesure afin d'avoir un suivi sur le moyen ou long terme. Cela vous permettra aussi de voir quelles sont les habitudes que vous avez gardé et où vous avez échoué. Si vous vous trouvez dans le second cas, posez-vous quelques questions avant d'abandonner définitivement l'idée. Que pouvez-vous changez pour que l'habitude reste ? Je vous invite à lire ma review d'[Atomic Habits]({{< ref "../books/atomic_habits/" >}}) concernant les habitudes.

J'espère que cette review vous aura donné quelques pistes d'amélioration de vos habitudes et fait réfléchir sur certains aspects de votre vie sans vous démoraliser ! N'hésitez pas à faire plus de recherches sur les points qui vous paraissent essentiels et où vous voudriez obtenir plus de détails.


