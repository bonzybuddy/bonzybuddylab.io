---
title: "Le parti pris des oiseaux"
date: 2022-03-06T10:51:06+01:00
draft: false
genres: 
- Biographie
- Roman
- Voyage
author: "Stanislaw Lubienski"
fiction: true
release_year: "2016"
duree:
# maj: "08 Fév 2021"
#cover: 
  img: /covers/DerekSivers-AnythingYouWant-cover.jpg
  source: https://sive.rs/a/
#toc: false
---
## 🗣 De quoi ça parle

La vie de l’auteur à travers sa passion pour les oiseaux. Il raconte cela simplement en évoquant comment celle-ci s’est manifestée puis développée. Il parle des voyages qu’il a fait pour observer certaines espèces ou celles présentes en Pologne à Varsovie. 

On ressent l’amour qu’il éprouve pour les oiseaux et le temps qu’il leur consacre. 

## 🔎 Comment je l'ai découvert

Je l’ai reçu à mon anniversaire.

## 💭 Ce que j'en ai pensé

J’ai beaucoup aimé ce livre. En plus de m’initier à l’observation des oiseaux dans ma vie quotidienne, il m’a appris de nombreuses anecdotes et informations sur les oiseaux en général.

Le livre regorge de sources extérieures que vous pourrez consulter en parallèle à la lecture.

## 🎯 Qui l'apprécierait

En premier lieu, les amateurs d’oiseaux (les *birdwatchers*) bien sûr, mais également les non initiés comme moi, qui n’ont (n’avaient) pas d’attirance particulière pour les oiseaux. 

Je pense que tout le monde apprécierait le livre car il est simple à lire, intéressant et utile. Il vous permettra de prendre conscience de certaines choses auxquelles vous ne faisiez peut-être pas attention auparavant.

## 🔗 Œuvres reliées

Des ouvrages consacrés aux oiseaux comme les encyclopédies et autres livres scientifiques dont l’auteur fait part dans son livre. 

D’autres livres dans la même veine où un auteur décrit sa passion et tente de la partager avec ses lecteurs.