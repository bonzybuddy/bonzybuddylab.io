---
title: "Zero to One"
date: 2021-04-23T16:30:33+02:00
# maj: "08-02-2021"
draft: false
genres:
  - Business
  - Créativité
  - Life Advice
  - Productivité
author: "Peter Thiel"
fiction: false
release_year: "2014"
cover:
  img: https://upload.wikimedia.org/wikipedia/en/thumb/d/d3/Zero_to_One.jpg/220px-Zero_to_One.jpg
  source: https://en.wikipedia.org/wiki/Zero_to_One
---

## 🥇🥈🥉 Le livre en 1 phrase

- Monter sa startup : conseils sur le déroulement et erreurs à éviter

## 🎣 Impressions

Ce livre est écrit par Peter Thiel, le co-fondateur de PayPal et traite essentiellement du monde des startups. Même s'il ne m'a pas personnellement impliqué car ce n'est pas ce que je souhaite faire actuellement, le livre peut aider toute personne cherchant à développer sa propre startup. L'auteur balaie les différents aspects de la startup, allant de l'argent aux employés en passant par l'idée originale et la concurrence. Il compare différentes startups qui ont réussi et qui ont échoué, en expliquant pourquoi elles n'ont pas réussi. En lisant ce livre vous aurez alors l'avis d'un entrepreneur qui a très bien géré son affaire.

## 🎯 Qui devrait le lire

Tous ceux qui souhaitent monter leur startup et qui aimeraient des conseils sur comment faire.
