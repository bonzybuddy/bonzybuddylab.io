---
title: "Arsène Lupin"
date: 2021-11-15T09:41:53+01:00
draft: false
genres: 
- Aventure
- Roman
author: "Maurice Leblanc"
fiction: true
release_year: "1874"
duree:
# maj: "08 Fév 2021"
#cover: 
  img: /covers/DerekSivers-AnythingYouWant-cover.jpg
  source: https://sive.rs/a/
#toc: false
---
## 🗣 De quoi ça parle

Arsène Lupin est le personnage principal de plusieurs livres de Maurice Leblanc ce qui rend impossible de résumer toutes les histoires. 

Néanmoins, Arsène Lupin est un prestigieux et célèbre cambrioleur français du XIXème siècle. Il effectue des vols de grande envergure notamment dans les milieux bourgeois et aristocrates. Ses vols sont rendus possible par sa maîtrise des déguisements et du maquillage qui lui permettent de jouer plusieurs rôles à la fois. En plus d'être particulièrement ingénieux et malin, il a établi un vaste réseau de collaborateurs infiltrés dans la plupart des institutions publiques ou privées. Personne ne sait réellement qui fait partie de sa bande ce qui rend son arrestation d'autant plus compliquée. Il s'attaque aux plus grands trésors de France ce qui participe à sa notoriété ; il est même admiré par l'inspecteur Ganimard chargé de l'arrêter. Son plus grand ennemi est Herlock Sholmes, un détective anglais présentant, vous l'aurez deviné, une similitude troublante avec Sherlock Holmes. 

Parmi la multitude de livres, les premières histoires se suivent mais d'autres ne font même pas intervenir Arsène Lupin. L'univers ainsi créé par l'auteur est très développé ce qui a donné lieu à de nombreuses adaptations du théâtre au cinéma en passant par la bande dessinée.

## 🔎 Comment je l'ai découvert

Ou plutôt redécouvert ; la sortie de la série sur Netflix m'a donné envie de lire les livres pour mieux connaître ce célèbre cambrioleur.

## 💭 Ce que j'en ai pensé

J'ai bien aimé le côté policier et enquêtes des livres. Ce sont des histoires divertissantes à suspens ce qui explique sa popularité. J'ai surtout aimé les premiers livres où l'intrigue est bien ficelée avec des éléments qui se rejoignent pour concorder à la fin. On découvre la vie du héros peu à peu ce qui laisse toujours une part de mystère. Le protagoniste est attachant, et malgré son caractère bien à lui, il est si puissant, presque invincible que l'on ne peut s'empêcher de l'admirer. Du fait de son intelligence, il se joue de ses adversaires, les humiliant en s'échappant à leur nez et à leur barbe. Il a toujours un coup d'avance en plus d'être prévoyant et intrépide. Toutes ces qualités font que le lecteur a bien envie de s'identifier à ce personnage énigmatique.

J'ai cependant moins aimé les derniers livres et notamment ceux où il n'est même pas question du héros car les histoires sont donc bien moins intéressantes. Je n'ai d'ailleurs pas très bien compris pourquoi elles appartiennent à la série consacrée à Arsène Lupin. 

Hormis cela, je pense qu'il est bon de lire les histoires principales pour découvrir Arsène Lupin, qui sont les 5 premiers livres et les 2 derniers.

## 🎯 Qui l'apprécierait

Evidemment ceux qui aiment les romans policiers avec des énigmes, des enquêtes, des rebondissements et beaucoup de dialogues. 

Ceux qui aiment les personnages avec une histoire très développée.

## 📚 Livres reliés

Sherlock Holmès de Sir Arthur Conan Doyle ou encore les livres d'Edgar Allan Poe.