---
title: "1984"
date: 2021-04-18T12:22:36+02:00
#maj: "08-02-2021"
draft: false
genres:
  - Sci-Fi
  - Roman
author: "George Orwell"
fiction: true
release_year: "1949"
cover:
  img: https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/1984first.jpg/220px-1984first.jpg
  source: https://en.wikipedia.org/wiki/Nineteen_Eighty-Four
---

## 🗣 De quoi ça parle

Une société totalitaire au XIXe siècle. L'histoire principale se déroule à Londres en 1984 et il n'existe plus que 3 états qui se partagent tous les territoires disponibles sur Terre. L'état dans lequel vit Winston, le protagoniste, est dirigé par Big Brother, le leader du "Parti" nom donné au gouvernement. Winston travaille pour le Parti mais hait la manière dont ils dirigent la société. En effet, le Parti surveille les moindres faits et gestes de tous les citoyens au moyen de caméras de surveillance, de micros et d'espions. Même les idées et la création sont interdites. Winston aimerait s'engager dans le parti révolutionnaire de _La Fraternité_ afin de changer le monde mais ne sait comment les contacter car il est dangereux de vouloir s'émanciper. Il va faire la rencontre d'une personne qui va changer sa vie.

{{% spoiler %}}
Il tombe amoureux de cette femme et la voit en secret. Ils sont ensuite
contactés par un de leurs collègues qui les enrôle dans
<em>La Fraternité</em>. Durant 7 ans ils pensent mener un double jeu mais se
font finalement arrêter par l'homme qui s'était fait passer pour un chef du
parti révolutionnaire. Winston est alors enfermé pendant un temps indéfini
avant de se faire torturer et laver le cerveau dans le but de lui faire
aimer Big Brother et le Parti. Le personnage de O'Brien (le faux chef
révolutionnaire qui est en réalité le chef de la police d'espionnage) est
absolument détestable. Winston est finalement relâché après avoir avoué et
confessé tous ses pêchés. Il finit par aimer Big Brother malgré lui, fin du
livre.
{{%/ spoiler %}}

## 🔎 Comment je l'ai découvert

Un classique de la littérature anglaise, traduit dans de nombreuses langues. C'est également un livre reconnu pour sa vision futuriste.

## 💭 Ce que j'en ai pensé

J'ai trouvé le livre assez long et ennuyeux pour ce qu'il raconte. L'histoire principale est assez peu développée, il n'y a que la fin qui m'a plu car c'est le dénouement et la confrontation des personnages principaux. Aucun personnage n'est attachant mais cela est sûrement voulu par l'auteur. Le monde décrit est absolument horrible et personne n'aimerait y vivre. Il y a très peu de dialogues et les descriptions sont souvent (très) longues. Ce n'est pas un livre joyeux mais qui au contraire, fait réfléchir sur l'avenir de notre société et sur ce qu'elle pourrait devenir. On peut voir le livre comme une sorte d'avertissement sur le sort de cet avenir. J'ai donc plutôt bien aimé le fond mais pas la manière dont l'histoire est amenée.

## 🎯 Qui l'apprécierait

Ceux qui apprécient les livres d'anticipation dystopiques. Les lecteurs qui aiment les longues descriptions.

## 📚 Livres reliés

"La ferme des animaux" du même auteur ou encore les livres décrivant des mondes futurs comme le livre d'Aldous Huxley "Le Meilleur des mondes"


