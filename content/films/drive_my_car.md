---
title: "Drive My Car"
date: 2021-10-10T15:51:58+02:00
# maj: "08-02-2021"
draft: false
author: "Ryûsuke Hamaguchi"
principal_actors: "Hidetoshi Nishijima, Tôko Miura, Masaki Okada"
genres:
  - Drame
release_year: "2021"
duree: "3h"
cover:
  img: https://m.media-amazon.com/images/M/MV5BNmZlZTRhZDMtOGZlMy00YmYxLTk5MmEtMTJjMjk3OTJhODJiXkEyXkFqcGdeQXVyODA0MjgyNzM@._V1_.jpg
  source: https://www.imdb.com/title/tt14039582/?ref_=fn_al_tt_1
---

_Cette review est extraite de mon [Top 10 des films d'octobre 2021]({{< ref "/films/top10.md#drive-my-car" >}})_

Comme je me fie aux affiches avant d'aller voir un film, je m'attendais à tout autre chose de Drive My Car. Je pensais que c'était un film noir sur la mafia japonaise ou dans ce style. Pas du tout.

Drive My Car est un film japonais et contrairement au titre, a le théâtre comme thème principal. L'histoire s'inspire d'une nouvelle éponyme écrite par Haruki Murakami et relate la vie d'un metteur en scène recruté pour la mise en scène d'Oncle Vania jouée lors d'un festival à Hiroshima. Le début du film se passe dans son appartement avec sa femme qui écrit des séries et des programmes pour enfants à la télévision japonaise. Elle a l'habitude d'inventer des histoires étranges lorsqu'ils font l'amour et qui sont les points de départ de ses programmes. Lui a l'habitude répéter ses textes en conduisant lors de ces trajets domicile-travail car il est aussi acteur dans ses propres pièces. Mais un terrible événement se produit et la deuxième partie du film se passe 10 ans plus tard.

Le début du film me fait un peu penser à Julie (en 12 chapitres) avec cette histoire de couple, et cette version originale japonaise qui implique davantage dans le film. Mais la similitude s'arrête là. Drive My Car est un film qui est joué comme une pièce de théâtre. J'ai d'ailleurs eu du mal à différencier les moments de cinéma et ceux de théâtre. Mais cela est fait exprès et nous plonge dans deux univers à la fois. Les expressions des personnages sont très importantes ainsi que leur langage corporel. Chaque personnage a son propre intérêt et apporte quelque chose au film. Il n'y a presque (voire pas du tout) de musique, et le film se déroule alors dans une grande quiétude. Certaines scènes sont vraiment longues dont plusieurs se déroulent dans la voiture, une Saab 900 Turbo que le protagoniste bichonne et vénère. On pourrait (un peu) le comparer à First Cow, pour cette ode à la lenteur et à la contemplation. Cependant ces 3h m'ont paru plutôt longues. Un beau film néanmoins.