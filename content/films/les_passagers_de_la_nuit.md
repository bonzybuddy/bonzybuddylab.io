---
title: "Les Passagers de la nuit"
date: 2022-06-03T20:02:32+02:00
# maj: "08 Fév 2021"
draft: false
author: "Mikhaël Hers"
principal_actors: "Charlotte Gainsbourg, Quito Rayon Richter, Noée Abita"
genres:
  - Drame
release_year: "2022"
duree: "1h51"
cover:
  img: https://m.media-amazon.com/images/M/MV5BN2ZmM2NjMjYtNDJmYy00NjA1LTgyZTItYjhiMTdjY2MyOTYwXkEyXkFqcGdeQXVyMTMwNjQxNDU1._V1_.jpg
  source: https://www.imdb.com/title/tt13846402/?ref_=nv_sr_srsg_0
#toc: false
---

## 🗣 De quoi ça parle

L’histoire se passe dans le quartier de Beaugrenelle à Paris dans les années 80. Elisabeth vient de se faire quitter par son mari et doit dorénavant s’occuper seule de ses deux enfants. Elle n’a jamais travaillé ce qui pose problème pour continuer à vivre correctement. Après quelques tentatives ratées, elle trouve un poste de standardiste radio pour l’émission “Les Passagers de la nuit” animée par Vanda où des auditeurs appellent pour partager leur parcours de vie. Après le témoignage touchant de Talulah, une jeune vagabonde arrivée à Paris un peu par hasard, Elisabeth décide de l’héberger pour ne pas la laisser dans la rue. Ce choix va alors venir bouleverser le quotidien de la famille.

## 🔎 Comment je l'ai découvert

Dans le programme de ciné du lundi soir.

## 💭 Ce que j'en ai pensé

L’atmosphère planante du film m’a particulièrement touché. Les scènes tournées dans le studio de radio m’ont confirmé que cette ambiance correspond à ce que j’aime. Le monde de la radio en général me plaît comme j’ai pu le souligner dans “Les Magnétiques”. La majorité du film se déroule dans le salon de l’appartement qui donne sur le quartier Beaugrenelle et la Seine et malgré la situation familiale bancale on s’y sent à l’aise.

J’ai cependant beaucoup moins apprécié les passages mélodramatiques tristes qui occupent malgré tout une bonne partie du scénario. On est bien dans le genre du drame et peut-être un peu trop à mon goût.

Enfin le film marque un point supplémentaire pour sa musique de très bonne facture.

## 🎯 Qui l'apprécierait

Ceux qui ont l’habitude d’écouter des émissions de nuit à la radio avec cette ambiance particulière du monde de la nuit, calme et apaisante.

## 🔗 Œuvres reliées

Le film [Les Magnétiques]({{< ref "./les-magnetiques.md/" >}}) pour le thème de la radio mais aussi pour cette description de la jeunesse que l’on retrouve au centre des deux films.

J’ajouterai également [Les Olympiades]({{< ref "./les-olympiades.md/" >}}), où le quartier dans lequel évoluent les acteurs joue un rôle bien présent. Ce sont d’ailleurs deux quartiers du sud de Paris présentant des similitudes architecturales ; le quartier des Olympiades dans le 13ème arrondissement et de celui de Beaugrenelle dans le 15ème. De plus, la jeunesse est encore une fois au coeur du scénario dans ces deux films.
