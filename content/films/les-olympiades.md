---
title: "Les Olympiades"
date: 2021-11-06T10:38:12+01:00
# maj: "08 Fév 2021"
draft: false
author : "Jacques Audiard"
principal_actors: " Lucie Zhang, Makita Samba, Noémie Merlant et Jehnny Beth"
genres:
  - Drame
release_year: "2021"
duree : "1h45"
cover: 
  img: https://m.media-amazon.com/images/M/MV5BM2JhMzFkNzgtOTFjZC00ZmY5LWFmZjAtMjY5YTdkOWRkYjdlXkEyXkFqcGdeQXVyMTMwNjQxNDU1._V1_.jpg
  source: https://www.imdb.com/title/tt12708658/?ref_=nv_sr_srsg_0
#toc: false
---

## 🗣 De quoi ça parle

Adapté d'une série de bandes dessinées d'Adrian Tomine, le film retrace l'histoire de quatre jeunes trentenaires vivant dans le quartier des Olympiades à Paris. Ces quatre personnages vont se rencontrer et leurs destins vont peu à peu se retrouver liés.

Le film aborde principalement le thème de la sexualité.

## 🔎 Comment je l'ai découvert

Grâce aux bandes-annonces avant les séances et à l'affiche.

## 💭 Ce que j'en ai pensé

Coup de cœur pour la musique. Composée par Rone un de mes artistes électro préférés, la bande originale du film est envoutante et planante. En sortant de la séance, j'ai écouté en boucle la musique du générique. 

J'ai beaucoup apprécié que le film soit tourné en noir et blanc. Je trouve que cela apporte un côté original et décalé. Les premiers plans du film avec des vues aériennes sur Paris et sur ce quartier des Olympiades sont très belles et m'ont rappelé beaucoup de souvenirs.

Interprété par Makita Samba, j'ai particulièrement apprécié le personnage de Camille. De manière générale, tous les personnages sont attachants ce qui implique le spectateur dans le film. 

Les dialogues sont parfois désinvoltes mais souvent teintés d'humour et on y retrouve la caractéristique de Michel Audiard, père du réalisateur et grand dialoguiste du cinéma français des années 1960. 

## 🎯 Qui l'apprécierait

Les jeunes d'aujourd'hui. Ceux qui aiment la ville de Paris car de nombreuses scènes sont tournées dans la rue ou dans le métro. 

## 📽 Films reliés

Les autres films de Jacques Audiard et peut-être aussi d'une certaine manière les vieux films en noir et blanc sur la jeunesse parisienne.

