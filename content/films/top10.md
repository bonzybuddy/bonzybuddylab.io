---
title: "Mon top 10 des films octobre 2021"
date: 2021-10-28T13:07:55+02:00
draft: false
toc: true
---

Pendant ce mois d'octobre j'ai eu la chance de bénéficier d'une offre culturelle proposée par l'Université permettant d'aller gratuitement au cinéma. Cette période coïncide également avec la sortie des films présentés au festival de Cannes ce qui promet toujours de belles découvertes. J'en ai donc profité pour en voir un maximum ce qui m'amène à écrire cet article.
Celui-ci reprend un par un les 10 films que j'ai vu avec un petit résumé et mon opinion personnelle. Mon but ici est surtout de partager mon point de vue.

## Tralala

Ce n'est pas par hasard si je place Tralala au début de cette liste. C'est sans doute le film que j'ai le plus apprécié parmi tous et je n'hésiterai pas à le classer comme mon film préféré de l'année. Tralala est pourtant une comédie musicale et je pensais ne pas aimer ce style où le rythme du film est interrompu par les chorégraphies et les chansons. Après avoir vu Tralala, je sais que je peux remettre en cause mon jugement.

Le film suit un mendiant appelé Tralala qui joue de la guitare dans les rues de Paris pour gagner sa vie. Les premières scènes ne sont d'après moi pas les meilleures et il faut s'accrocher un peu pour rester dans le film. Tralala fait la rencontre d'une jeune fille devant la gare Montparnasse qu'il prend pour une apparition divine. Entre rêve et réalité et après un verre en terrasse, elle disparaît soudainement en laissant comme souvenir un briquet provenant de la ville de Lourdes. Tralala décide alors de s'y rendre pour la retrouver. Une fois sur place, en plus de retrouver sa muse, il va être embarqué au cœur d'une histoire de famille...

J'ai vraiment presque tout adoré du film. Premièrement la musique, qui mélange chanson française, disco, rock et même un peu de rap. J'ai grandement apprécié tous ces moments de chanson accompagnés de chorégraphies parfois éblouissantes. Les couleurs de certaines scènes sont incroyables, les acteurs sont attachants et la fin bien que prévisible est émouvante. C'est un film qui m'a rendu très heureux et joyeux en sortant de la salle, ce qui achève ma note plus que positive. Un sans-faute selon moi.

_Pour plus d'infos cliquez [ici]({{< ref "/films/tralala.md" >}})_

## Julie (en 12 chapitres)

Je ne pensais pas aller voir ce film car l'affiche ne m'attirait pas. Mais en lisant plusieurs critiques très positives je me suis dit qu'il serait dommage de passer à côté d'une éventuelle pépite. Je n'ai pas été déçu ! Je peux même dire que j'ai adoré ce film et je vous le recommande fortement.

L'histoire nous plonge dans la vie d'une jeune norvégienne du nom de Julie à travers 12 chapitres plus ou moins longs. Comme dans un livre, chaque chapitre se concentre sur un événement. Julie est une trentenaire qui depuis ses années universitaires se cherche, trouve, doute, puis recommence à chercher. Après plusieurs relations, elle rencontre un dessinateur de BD reconnu avec qui elle commence à partager sa vie.

On se doute un peu de la suite des évènements mais, comme je disais en introduction, cette intrigue a priori banale et déjà-vu est amenée d'une si belle manière par Joachim Trier qu'elle en devient un chef-d'œuvre.
Le duo d'acteurs principaux est extraordinaire. Renate Reinsve interprète merveilleusement le rôle de Julie qui lui a d'ailleurs valu le prix de l'interprétation féminine à Cannes. J'ai aussi adoré Anders Danielsen Lie, également impressionnant dans son rôle et j'ai hâte de découvrir ses autres films. Enfin, j'ai beaucoup aimé le fait d'avoir vu le film en norvégien ce qui est atypique et change radicalement de la sonorité anglaise pour apporter une touche d'autant plus originale. Concernant la sonorité, la musique est bien présente, elle rythme le film en fonction de ses hauts, ses bas et le mariage des deux est formidable. Je me suis même surpris à réécouter la bande originale en sortant de la séance pour ne pas être coupé de l'univers du film. N'attendez plus pour le voir.

_Pour plus d'infos cliquez [ici]({{< ref "/films/julie.md" >}})_

## First Cow

First Cow est un western mais qui ne ressemble à aucun autre. Il n'a du western traditionnel que l'époque et le lieu, autrement dit l'Amérique au XIXe siècle. First Cow est adapté du roman _The Half-Life_ de Jonathan Raymond. J'ai trouvé la bande-annonce peu représentative du film en plus de dévoiler une bonne partie de l'intrigue. C'est la raison pour laquelle je n'aime pas regarder les bandes-annonces ni lire les synopsis avant de voir un film.

L'histoire suit un cuisinier faisant la rencontre d'un immigré chinois qui s'associent dans le but de gagner leur vie. Ils trouvent une idée qui leur permet de gagner pas mal d'argent. Cependant, l'élément clé de leur réussite repose sur la vache du chef de la colonie. Cette vache est d'ailleurs la première vache importée dans l'état d'Oregon d'où le titre du film. Malheureusement, un élément va alors faire basculer leur projet.

La première chose qui frappe est que le film est tourné au format 1,37 c'est-à-dire que l'image est presque carrée et dénote une fois de plus avec les westerns traditionnels aux plans très larges. Le film est lent mais au sens tranquille et reposant. La musique de William Tyler avec ses airs de country est peu présente mais le film n'en pâtit pas. Il n'y a aucune brutalité contrairement aux westerns classiques ; il n'y pas l'omniprésence des armes et du culte de l'homme viril. Ici, place à la forêt vierge avec nos deux pionniers qui vivent leur vie du mieux qu'ils peuvent dans cette nature qu'est l'Amérique de l'époque. Un film simple en apparence mais poignant grâce à l'empathie des deux protagonistes. Une très belle découverte du cinéma indépendant américain.

_Pour plus d'infos cliquez [ici]({{< ref "/films/first_cow.md" >}})_

## Le Sommet des Dieux

Le Sommet des Dieux est une merveille de l'animation. A cause de l'animation justement, j'ai hésité avant d'aller le voir. Je serais vraiment passé à côté de quelque chose. Je pense que le manga aussi vaut le détour car il est très bien documenté sur l'alpinisme et ses techniques. L'histoire du manga est également plus développée que dans le film ce dernier ayant par ailleurs nécessité 8 ans de travail !

Le film est donc adapté du manga de Jirō Taniguchi, lui-même inspiré du roman de Baku Yumemakura sur l'alpinisme japonais dans les années 90. L'histoire est racontée et vue à travers les yeux d'un reporter photo de montagne qui suit les expéditions et immortalise leur succès avant de vendre ses clichés. Un élément déclencheur le met sur la trace d'Habu Jôji un alpiniste renommé qui a disparu de la scène internationale après d'illustres ascensions sur les faces les plus difficiles du monde. Ce mystérieux Habu détiendrait l'appareil photo original de George Mallory, premier alpiniste qui tenta d'atteindre le sommet de l'Everest en 1924 avec son compatriote Andrew Irvine, 30 ans avant la première réussite confirmée. Ils ont été aperçus pour la dernière fois à 245m du sommet avant de disparaître à jamais, sans que personne ne sache s'ils ont réussi. Le développement de la pellicule de l'appareil de Mallory permettrait alors de connaître la vérité...

Je trouve que le scénario est haletant et donne vraiment envie de connaître la suite. Cette intrigue autour de l'appareil photo mais aussi d'Habu est très bien ficelée. La première chose à relever concernant ce film est la beauté du dessin. En effet, le coup de crayon est splendide, c'est un plaisir pour les yeux. Les couleurs sont également très travaillées, avec cette dominante blanche de la neige himalayenne. La musique d'Amine Bouhafa atteint elle aussi des sommets et nous transporte dans l'action des personnages. A voir absolument.

_Pour plus d'infos cliquez [ici]({{< ref "/films/sommet.md" >}})_

## Mourir peut attendre

Etant un amateur de James Bond, la sortie de ce 25ème opus était un passage presque nécessaire. Voir un James Bond (et tout autre film d'action) au cinéma est toujours plus agréable et permet d'apprécier sa dimension à juste titre.

Le film reprend donc l'intrigue de Spectre avec un Daniel Craig en pré-retraite. Un virus super-puissant développé par les services secrets britanniques capable de cibler une ADN avant de frapper mortellement est dérobé par un méchant dont on ne sait pas grand-chose. 007 reprend alors du service pour le retrouver et sauver le monde. Toute l'intrigue est imbriquée entre l'organisation Spectre, le passé du docteur Swann (Léa Seydoux) et James Bond.

Je n'ai jamais été fan des scénarios de James Bond et ce film ne fait pas exception. Scénario catastrophe et James qui sauve la planète, c'est vu et revu. Pourtant, malgré ses 2h45 et son scénario un peu tiré par les cheveux, le film ne m'a pas paru trop long. Cela est dû en partie aux scènes d'action assez impressionnantes qui se déroulent au sein de paysages très beaux comme dans la neige, dans la poussière d'un petit village d'Italie ou encore dans la forêt en Finlande. Ces moments ont donc été mes préférés. Comme cette course poursuite sur une route finlandaise qui se termine dans une forêt brumeuse digne d'un film d'horreur avec cette angoisse de voir surgir un ennemi à tout moment. Une de mes scènes favorites est celle de Cuba avec Ana de Armas qui aurait dû selon moi jouer un rôle beaucoup plus important. J'ai donc apprécié certains pans du film, de même que la colorimétrie travaillée et convaincante comme dans le club jamaïcain où il retrouve son vieil ami Felix Leiter. J'ai cependant été déçu par la musique. Composée par Hans Zimmer je m'attendais à mieux et comme la musique est un point crucial pour me faire aimer un film, il perd ici quelques points. Le rôle du méchant est selon moi très médiocre. Rami Malek n'est pas convaincant et le développement du personnage manque de crédibilité.

_Pour plus d'infos cliquez [ici]({{< ref "/films/007.md" >}})_

## Drive My Car

Comme je me fie aux affiches avant d'aller voir un film, je m'attendais à tout autre chose de Drive My Car. J'imaginais un film noir sur la mafia japonaise ou dans ce style. Pas du tout.

Drive My Car est un film japonais dont le théâtre est le thème principal. L'histoire s'inspire d'une nouvelle éponyme écrite par Haruki Murakami et relate la vie d'un metteur en scène recruté pour la mise en scène d'"Oncle Vania" jouée lors d'un festival à Hiroshima. Le début du film se déroule dans son appartement avec sa femme qui écrit des séries et des programmes pour enfants à la télévision japonaise. Lui a l'habitude de répéter ses textes en conduisant lors de ses trajets domicile-travail car il est aussi acteur dans ses propres pièces. Mais un terrible événement se produit et la deuxième partie du film se passe 10 ans plus tard.

Le début du film me fait un peu penser à Julie (en 12 chapitres) avec cette histoire de couple, et cette version originale japonaise qui implique davantage dans le film. Mais la similitude s'arrête là. Drive My Car est un film qui est joué comme une pièce de théâtre. J'ai d'ailleurs eu du mal à différencier les moments de cinéma de ceux de théâtre. Mais cela est fait exprès et nous plonge dans deux univers à la fois. Les expressions des personnages sont très importantes ainsi que leur langage corporel. Chaque personnage a son propre intérêt et apporte quelque chose au film. Il n'y a presque (voire pas du tout) de musique, et le film se déroule dans une grande quiétude. Certaines scènes sont vraiment longues dont plusieurs se déroulent en voiture, une Saab 900 Turbo que le protagoniste bichonne et vénère. On pourrait (un peu) le comparer à First Cow, pour cette ode à la lenteur et à la contemplation. Cependant, ces 3h m'ont paru plutôt longues. Un beau film néanmoins.

_Pour plus d'infos cliquez [ici]({{< ref "/films/drive_my_car.md" >}})_

## Illusions perdues

Illusions perdues est le titre du roman de Balzac dont est adapté ce film de Xavier Giannoli. N'ayant pas lu le roman je n'ai aucun moyen de comparaison avec le film. C'est mon premier film de ce réalisateur dont j'ai entendu du bien.

Lucien de Rubempré est un jeune poète qui travaille dans une imprimerie de province. Grâce à la marquise de Bargeton qui joue le rôle de mécène pour les petits artistes, il monte à Paris dans le but de faire fortune en publiant ses poèmes. Malheureusement la vie parisienne est dure et cruelle et Lucien ne connaît pas les coutumes de cette nouvelle vie. Il finit par se faire engager dans un journal libéraliste.

Lucien de Rubempré est un personnage qui n'attire pas l'empathie. Il est rancunier et sans scrupules. Son talent est tout compte fait limité puisqu'il ne publiera jamais aucun poème hormis un petit recueil médiocre. On est donc plongé dans le monde du journalisme, du théâtre, de la littérature mais aussi de l'aversion entre l'ancienne aristocratie sur le déclin et la nouvelle bourgeoisie riche et puissante. Le film est relativement précurseur avec sa vision des journaux corrompus et mensongers qui écrivent n'importe quoi pourvu qu'ils vendent. On peut donc y voir le début de notre société capitaliste actuelle avec le pouvoir de l'argent. Pourtant, je n'ai pas vraiment aimé le film et les acteurs ne m'ont pas convaincus. A aucun moment je n'ai ressenti de l'empathie envers un personnage. Je me suis consolé en me disant qu'ils étaient écrits de telle manière par Balzac mais je n'en ai aucune idée. La musique ne m'a pas particulièrement marquée.

_Pour plus d'infos cliquez [ici]({{< ref "/films/illusions_perdues.md" >}})_

## Titane

Je voulais voir Titane car il s'agit du lauréat de la Palme d'or 2021. Je suis donc allé le voir sans chercher à savoir si le thème me plairait ; j'étais persuadé qu'une Palme d'or valait forcément la peine. J'ai (re)découvert le genre ultra-violent et noir, à la limite du fantastique que l'on retrouve dans _Under the Skin_ avec Scarlett Johannson.

Après un accident de voiture, Alexia subit une opération de la tête où les chirurgiens lui implantent une prothèse en titane. Quelques années plus tard, Alexia travaille en tant que danseuse dans un car show. Mais poussée par des pulsions, elle assassine plusieurs personnes avant d'être recherchée par toute la police de Marseille. Elle quitte alors son domicile familial et essaie de prendre l'apparence d'un jeune garçon disparu depuis 10 ans afin d'être adoptée par un nouveau père. Ce dernier est capitaine dans une caserne de pompiers et reconnaît en Alexia son fils disparu, Adrien. Alexia/Adrien est recueilli mais se mure dans un mutisme, ce les autres pompiers de la caserne ont du mal à l'accepter.

Une personne qui prend la place d'une autre disparue depuis des années, on retrouve là l'intrigue de Tralala qui n'a pourtant absolument rien à voir avec Titane. Cela démontre une fois de plus qu'une idée peut être travaillée de manière complètement opposée. Les acteurs sont formidables et malgré le peu de dialogues, beaucoup d'émotions passent par le langage non verbal. J'ai beaucoup aimé toutes les scènes de danse et de fête notamment celle au début dans ce car show ou dans la caserne. Les couleurs parfois excentriques et flashy, l'ambiance et la musique présentes m'ont happé et vraiment fait aimer ces quelques scènes. Dommage que je ne puisse pas en dire autant des autres où la violence est très présente, ce qui m'a heurté. Comme dans _Drive_ avec Ryan Gosling, j'ai du mal avec les scènes ultra-violentes. Je trouve qu'il faut quand-même s'accrocher par moments pour ne pas fermer les yeux et les bruitages n'aident pas. J'ai donc un avis assez mitigé.

_Pour plus d'infos cliquez [ici]({{< ref "/films/titane.md" >}})_

## Dune

On m'avait vendu Dune comme étant le film événement de l'année, un film magistral qui serait une pépite. J'y suis donc allé pour me faire mon propre avis, même si je n'apprécie pas trop les films de science-fiction.

Dune est adapté du roman éponyme de Frank Herbert qui retrace la vie du jeune Paul Atréides, fils d'une grande maison dirigeante sur une planète inconnue.

Je passe rapidement sur l'histoire où j'ai vite décroché en raison de la multitude d'éléments et de personnages. Dune m'a fait penser à deux autres grands succès du cinéma : Star Wars avec son univers parallèle, son Empereur et les différentes maisons gouvernant les planètes et Mad Max où la quasi-totalité du film se passe dans un désert de sable.
Dune se déroule dans une palette infinie de nuances de gris puis de jaune et d'ocre. L'univers ainsi créé est hostile mais il faut reconnaître la qualité du travail de la colorimétrie. Dune confirme mon intérêt limité pour les films de science-fiction. J'ai une fois de plus été déçu par la musique d'Hans Zimmer tout comme dans James Bond. C'est une musique épique avec des voix samplées mais qui n'est donc pas vraiment une musique. Je suis resté sur ma faim avec une fin qui ouvre sur une suite et laisse donc de nombreuses questions sans réponses. C'est dommage car cette première partie ne m'a pas suffisamment convaincue pour aller voir la deuxième si elle sortira un jour.

_Pour plus d'infos cliquez [ici]({{< ref "/films/dune.md" >}})_

## The French Dispatch

Je suis allé voir The French Dispatch uniquement en raison de la bande-annonce, qui m'a happé par ses couleurs, ses plans intrigants et son casting incroyable. D'habitude, j'essaie de ne pas regarder la bande-annonce ou lire le synopsis avant de voir un film. Je me fie uniquement à l'affiche, ce qui est tout aussi idiot mais bon, chacun sa méthode.

Le rédacteur en chef de The French Dispatch vient de décéder. Quatre journalistes racontent alors une histoire qu'ils ont écrite se déroulant à Ennui-sur-Blasé, une petite ville française dont The French Dispatch est le journal local.

Les dialogues et la voix-off censée nous expliquer le contexte vont si vite qu'il est vraiment compliqué de suivre le film. Les plans aussi vont trop vite alors qu'ils sont si riches en détails et mériteraient que la caméra s'y attarde davantage. A cause de cette frénésie, le spectateur est perdu, noyé au milieu d'un nombre considérable d'éléments. Il n'y a aucun personnage principal car les quatre histoires sont complètement indépendantes. Ce scénario alambiqué et cette absence de fil conducteur gâchent considérablement un film qui aurait pu être sublime. L'alternance de noir&blanc/couleur est vraiment bien, on passe d'un format 1,37 à un plan large, bref ça foisonne de bonnes idées. Chaque plan est un tableau, le travail de Wes Anderson est indéniable sur ce point mais la mise en scène devient lassante à la longue tellement elle est étriquée et carrée. La musique aussi est à l'honneur mais elle est trop souvent coupée par l'intervention d'un personnage. Le casting est pourtant incroyable, tous les acteurs du moment sont là : Chalamet, Seydoux, Brody, Wright, Amalric, del Toro, Murray, Khoudri, Waltz, Park, Wilson, Swinton ! La liste est interminable. Malheureusement, cela n'a pas réussi à me faire accrocher, et, à l'image du nom de la ville, je me suis ennuyé. De plus, tous les plus beaux éléments du film se retrouvent dans la bande-annonce qui supprime la surprise. Malgré de très bons éléments indéniables, j'ai donc été très déçu par The French Dispatch.

_Pour plus d'infos cliquez [ici]({{< ref "/films/french_dispatch.md" >}})_
