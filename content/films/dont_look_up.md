---
title: "Don’t Look Up"
date: 2022-03-13T01:50:58+01:00
# maj: "08 Fév 2021"
draft: false
author: "Adam McKay"
principal_actors: "Leonardo di Caprio, Jennifer Lawrence, Meryl Streep"
genres:
  - Comédie
  - Drame
  - Sci-Fi
release_year: "2021"
duree: "2h18"
cover:
  img: https://m.media-amazon.com/images/M/MV5BZjcwZjY3NjAtNzkxZS00NmFjLTg1OGYtODJmMThhY2UwMTc5XkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_.jpg
  source: https://www.imdb.com/title/tt11286314/
#toc: false
---

## 🗣 De quoi ça parle

La fin du monde est imminente à cause d’un astéroïde venant percuter notre planète et causant une extinction massive de l’humanité semblable à celle des dinosaures. C’est la découverte effrayante que fait une jeune doctorante en astronomie. Avec son professeur, ils se rendent à la Maison Blanche pour avertir la Présidente et essayer de trouver des solutions. Problème : elle s’en fout complètement et elle n’est pas la seule à être dans le déni. Les deux astronomes commencent alors une course contre-la-montre pour essayer d’alerter le monde et de faire bouger les choses.

## 🔎 Comment je l'ai découvert

Sur Internet et dans la presse spécialisée, il était cité parmi les films les plus attendus de l’année 2021.

## 💭 Ce que j'en ai pensé

L’apocalypse traitée de manière drôle et acerbe, avec un di Caprio comme souvent très convaincant.

Le rôle joué par le PDG de Bash fortement calqué sur Apple est hilarant mais aussi criant de vérité quand on s’aperçoit qu’il exerce plus de pouvoir sur les décisions que la Présidente elle-même.

Les effets rajoutés au montage sont très appréciables et contribuent au comique du film.

Don’t Look Up ne brille pas de par sa musique ou ses décors, ce n’est pas un film technique mais il cherche le fond et la prise de conscience.

## 🎯 Qui l'apprécierait

Ceux qui aiment les films sarcastiques et/ou à portée écologique, environnementale.

## 🔗 Œuvres reliées

Les films sur la fin du monde (sans effets spéciaux dans tous les sens).
