---
title: "L'Ombre d'un mensonge"
date: 2022-03-25T19:36:24+01:00
# maj: "08 Fév 2021"
draft: false
author: "Bouli Lanners"
principal_actors: "Michelle Fairley, Bouli Lanners, Andrew Still"
genres:
  - Drame
release_year: "2021"
duree: "1h39"
cover:
  img: https://m.media-amazon.com/images/M/MV5BNjdlNmExNGItYTg2Ny00NWE0LTliMWUtZTc5YzM4NWI5ZjFiXkEyXkFqcGdeQXVyMTMwNjQxNDU1._V1_.jpg
  source: https://www.imdb.com/title/tt11188010/
#toc: false
---

## 🗣 De quoi ça parle

Phil vit et travaille en Ecosse après avoir quitté sa Belgique natale. Il est victime d’un AVC qui lui fait perdre la mémoire. Millie, la fille du fermier qui emploie Phil, s’occupe de lui après son séjour à l’hôpital. Elle lui fait alors croire qu’ils avaient une relation amoureuse avant son accident et que personne n’était au courant. Phil retrouve peu à peu sa vie normale mais sa mémoire va-t-elle lui revenir ?

## 🔎 Comment je l'ai découvert

Dans les sorties de la semaine.

## 💭 Ce que j'en ai pensé

Commençons par parler des paysages. Tournés sur l’île de Lewis au nord-ouest de l’Ecosse, les plans en extérieur sont à couper le souffle devant l’immensité des espaces sauvages écossais où se côtoient mer, collines, landes et tourbières à perte de vue. On se sent réellement transporté dans un autre monde à travers ces paysages. Il n’y a quasiment aucune scène en ville ou dans un lieu animé. Même quand ils sont au bar, le cadrage est serré sur les personnages principaux et l’on ne voit pas le reste de la salle. Le seul autre lieu de rencontre des habitants de l’île est l’Église presbytérienne stricte et austère, qui vient compléter la description de l’île de Lewis.

Les émotions ressenties lors de la séance sont vives et profondes, on est bien dans le drame sentimental où le scénario joue sur le caractère tendre et fragile des personnages pour susciter cette compassion. J’ai eu des frissons à plusieurs moments et je trouve que cela traduit une réelle implication du spectateur dans le film.

Enfin, attardons-nous sur les acteurs. Ils ne sont qu’une poignée, accentuant cet effet de solitude omniprésent sans être dérangeant, et la quasi-totalité du film se concentre autour de Phil et de Millie. Ils n’en restent pas moins captivants et nous transmettent toutes les facettes des rôles qu’ils incarnent. Millie, cette femme célibataire désœuvrée et tourmentée, fait ressortir toutes ses émotions à travers son visage et ses gestes. Elle va finir par trouver refuge en la personne de Phil. Ce fameux Phil, joué par Bouli Lanners également réalisateur du film, est tout simplement incroyable. Son attitude désinvolte, mains vissées dans les poches, regard pénétrant rempli de secrets inavoués avec néanmoins une pointe d’humour et d’ironie reste conscient que sa vie ne tient qu’à un fil.

## 🎯 Qui l'apprécierait

Ceux qui aiment les films quelque peu contemplatifs avec peu de dialogues.
