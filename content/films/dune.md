---
title: "Dune"
date: 2021-10-18T15:51:35+02:00
# maj: "08-02-2021"
draft: false
author: "Denis Villeneuve"
principal_actors: "Timothée Chalamet, Rebecca Ferguson, Oscar Isaac"
genres:
  - Sci-Fi
  - Action
release_year: "2021"
duree: "2h35"
cover:
  img: https://m.media-amazon.com/images/M/MV5BN2FjNmEyNWMtYzM0ZS00NjIyLTg5YzYtYThlMGVjNzE1OGViXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg
  source: https://www.imdb.com/title/tt1160419/?ref_=nv_sr_srsg_0
---

_Cette review est extraite de mon [Top 10 des films d'octobre 2021]({{< ref "/films/top10.md#dune" >}})_

On m'avait vendu Dune comme étant le film événement de l'année, un film magistral qui serait une pépite. J'y suis donc allé pour me faire mon propre avis, même si je n'aime pas trop les films de science-fiction.

Dune est adapté du roman éponyme de Frank Herbert qui retrace la vie du jeune Paul Atréides, fils d'une grande maison dirigeante sur une planète inconnue.

Je passe rapidement sur l'histoire où j'ai vite décroché en raison de la multitude d'éléments et de personnages. Dune m'a fait penser à deux autres grands succès du cinéma : Star Wars et son univers parallèle, son Empereur et des différentes maisons gouvernant les planètes et Mad Max : Fury Road où la quasi-totalité du film se passe dans un désert de sable.
Dune se déroule dans une palette infinie de nuances de gris puis de jaune et d'ocre. L'univers ainsi créé est hostile mais il faut reconnaître la qualité du travail de la colorimétrie. Dune confirme mon intérêt limité pour les films de science-fiction. J'ai une fois de plus été déçu par la musique d'Hans Zimmer tout comme dans James Bond. C'est une musique épique avec des voix samplées mais qui n'est donc pas vraiment une musique. Je suis resté sur ma faim avec une fin qui ouvre sur une suite et laisse donc de nombreuses questions sans réponses. C'est dommage car cette première partie ne m'a pas suffisamment convaincue pour aller voir la deuxième si elle sortira un jour.