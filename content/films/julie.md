---
title: "Julie (en 12 chapitres)"
date: 2021-10-16T15:51:40+02:00
# maj: "08-02-2021"
draft: false
author: "Joachim Trier"
principal_actors: "Renate Reinsve, Anders Danielsen Lie, Herbert Nordrum"
genres:
  - Drame
release_year: "2021"
duree: "2h"
cover:
  img: https://m.media-amazon.com/images/M/MV5BZmNmNWM5ZGMtYjM0My00NjliLWFlODYtZTY0ZTYzNjgxYjExXkEyXkFqcGdeQXVyMTAyMjQ3NzQ1._V1_.jpg
  source: https://www.imdb.com/title/tt10370710/?ref_=nv_sr_srsg_0
---

_Cette review est extraite de mon [Top 10 des films d'octobre 2021]({{< ref "/films/top10.md#julie-en-12-chapitres" >}})_

Je ne pensais pas aller voir ce film car l'affiche ne m'attirait pas. Mais en lisant plusieurs critiques très positives je me suis dit qu'il serait dommage de rater une éventuelle pépite. Je n'ai pas été déçu ! Je peux même dire que j'ai adoré ce film et je vous le recommande fortement.

L'histoire nous plonge dans la vie d'une jeune norvégienne du nom de Julie à travers 12 chapitres plus ou moins longs. Comme dans un livre, chaque chapitre se concentre sur un événement. Julie est une trentenaire qui depuis ses années universitaires se cherche, trouve, doute, puis recommence à chercher. Après plusieurs relations, elle rencontre un dessinateur de BD reconnu avec qui elle commence à partager sa vie.

On se doute un peu de la suite des évènements mais, comme je disais en introduction, cette intrigue a priori banale et déjà-vu est amenée d'une si belle manière par Joachim Trier qu'elle en devient un chef-d'œuvre.
Le duo d'acteur principaux est extraordinaire. Renate Reinsve interprète merveilleusement le rôle de Julie qui lui a d'ailleurs valu le prix de l'interprétation féminine à Cannes. J'ai aussi adoré Anders Danielsen Lie, également impressionnant dans son rôle et j'ai hâte de découvrir ses autres films. Enfin, j'ai beaucoup aimé le fait d'avoir vu le film en norvégien ce qui est atypique et change radicalement de la sonorité anglaise pour apporter une touche d'autant plus originale. Concernant la sonorité, la musique est bien présente et rythme le film en fonction de ses hauts et ses bas et le mariage des deux est formidable. Je me suis même surpris à réécouter la bande originale en sortant de la séance pour ne pas être coupé de l'univers du film. N'attendez plus pour le voir.


