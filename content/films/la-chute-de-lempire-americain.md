---
title: "La Chute de l'empire américain"
date: 2021-11-12T13:59:15+01:00
# maj: "08 Fév 2021"
draft: false
author : "Denys Arcand"
principal_actors: "Alexandre Landry, Maripier Morin, Maxim Roy"
genres:
  - Comédie dramatique
release_year: "2018"
duree : "2h07"
cover: 
  img: https://m.media-amazon.com/images/M/MV5BMDRjODBiYmMtYTMyMC00OWFkLWIxOWYtYmMzOTczZDVjMTg5XkEyXkFqcGdeQXVyMTY0Mzg3MTY@._V1_.jpg
  source: https://www.imdb.com/title/tt7231342/
#toc: false
---

## 🗣 De quoi ça parle

Pierre-Paul Daoust est docteur en philosophie mais exerce le métier de livreur à Montréal. Il assiste malgré lui à un hold-up manqué et sanglant. Seul et désemparé, il récupère alors l'argent s'élevant à plusieurs millions de dollars, juste avant que la police n'arrive et ne l'interroge. Il réussit à cacher l'argent mais ne sait pas quoi en faire. Il décide alors de demander de l'aide à un ancien fraudeur qui vient de sortir de prison. Avec l'aide d'une escort girl, ils vont tous les trois tenter de blanchir l'argent dérobé tout en déjouant l'enquête de la police.

## 🔎 Comment je l'ai découvert

Dans [Télérama](https://www.telerama.fr/) à sa sortie.

## 💭 Ce que j'en ai pensé

Mon coup de cœur de l'année 2018 ! 

C'est un film magistral où tous les acteurs sont excellents. Leur jeu les rend attachants et on est heureux de les voir réussir. Le personnage de Pierre-Paul Daoust est maladroit mais c'est ce qui le rend touchant. Le film est empreint de suspens jusqu'à la fin ce qui tient le spectateur en haleine.

Les dialogues sont plein de réflexion et visent juste. Les rôles habituels sont inversés ; les hors-la-loi deviennent les gentils et la police prend la place des méchants.

Le scénario est pour une fois novateur et porte une vision critique de l'argent et sur la société capitaliste nord-américaine reposant sur la puissance du dollar. 

## 🎯 Qui l'apprécierait

Ceux qui aiment les films satiriques et les accents québécois.

## 📽 Films reliés

Les autres films de Denys Arcand, comme le Déclin de l'empire américain.

