---
title: "Babylon"
date: 2023-03-27T16:46:29+02:00
# maj: "08 Fév 2021"
draft: false
author: "Damien Chazelle"
principal_actors: "Brad Pitt, Margot Robbie, Diego Calva"
genres:
  - Comédie
  - Drame
  - Histoire
release_year: "2022"
duree: "3h09"
cover:
  img: https://m.media-amazon.com/images/M/MV5BZGIyMmE4YjYtY2FkYi00OTZiLWI1OGYtNjM4MTcxZmIwMjc4XkEyXkFqcGdeQXVyMTA3MDk2NDg2._V1_.jpg
  source: https://www.imdb.com/title/tt10640346/?ref_=nv_sr_srsg_2
#toc: false
---

Pour mon retour dans les salles obscures après près d’un an d’absence, j’ai profité du [Printemps du Cinéma](https://www.printempsducinema.com/). Après The Fabelmans et Mon Crime, j’ai sélectionné Babylon. Avec l’affiche, c’est le duo Brad Pitt/Margot Robbie qui m’ont convaincu.

Malgré les échos négatifs sur ce film de Damien Chazelle (à qui l’on doit Whiplash et La La Land), je m’étais mis en tête de me faire ma propre idée de cette longue fresque — 3h09 — sur les débuts d’Hollywood des années 20-30.

Tout d’abord, je tiens donc à mettre en avant la mise en scène magistrale, et le mot est pesé, qui crève l’écran du début à la fin. J’aurais aussi pu utiliser flamboyante, pour montrer à quel point il s’agit de quelque chose de travaillé. Entre les décors et les costumes, on se retrouve réellement plongé, happé dans ces Etats-Unis de l’Entre-deux-guerres. Cette période charnière pour le cinéma avec l’avènement du parlant a lancé une véritable ruée vers la notoriété de la part des producteurs et acteurs de l’époque. Cette ascension vers la gloire, teintée de drogue et de fêtes ostentatoires est ici parfaitement représentée. Tout comme le déclin, aussi brutal que leur progression, passant en un éclair de la lumière à l’oubli total avant même de s’en rendre compte. Chacun finira d’ailleurs par en payer les lourds tributs : addiction, pauvreté, suicide.

Le film retrace donc le parcours de trois personnages entre un acteur/producteur déjà bien implanté et deux autres, rêvant d’une carrière hollywoodienne. Ils se retrouveront tous liés à un moment ou un autre pour le meilleur ou pour le pire.

Il serait impossible de conclure cette publication sans mettre en lumière Margot Robbie alias Nellie LaRoy qui fait définitivement partie des plus grandes actrices actuelles. Son personnage au caractère sauvage mêlé à son ambition de gloire absolue, s’auto-proclamant star est aussi déconcertant qu’attachant. Sa frénésie trépidante dynamite le film tandis que le calme et la réflexion de Manny Torres interprété par la découverte Diego Calva adoucit l’ambiance.

La musique n’est pas en reste grâce au trompettiste Sidney Palmer, directement inspiré de Curtis Mosby qui vient rajouter une ambiance festive déjà très présente.

Il serait impossible de conclure cette publication sans mettre en lumière Margot Robbie alias Nellie LaRoy qui fait définitivement partie des plus grandes actrices actuelles. Son personnage au caractère sauvage mêlé à son ambition de gloire absolue, s’auto-proclamant star est aussi déconcertant qu’attachant. Brad Pitt, également très à l’aise dans son rôle ne déçoit pas et son charisme reste légendaire.

J’ai donc du mal à comprendre les critiques négatives lues par-ci, par-là car selon il s’agit déjà d’un grand film.
