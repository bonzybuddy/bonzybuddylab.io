---
title: "Les Magnétiques"
date: 2022-03-08T01:19:55Z
# maj: "08 Fév 2021"
draft: false
author : "Vincent Maël Cardona"
principal_actors: "Thimotée Robart, Marie Colomb, Joseph Olivennes"
genres:
  - Drame
release_year: "2021"
duree : "1h38"
cover: 
  img: https://m.media-amazon.com/images/M/MV5BOWY4NjUzOTYtNjg2NC00MjY2LWI4MTAtOTEyN2EzMzM5ZDFhXkEyXkFqcGdeQXVyODA0MjgyNzM@._V1_.jpg
  source: https://www.imdb.com/title/tt14068864/
#toc: false
---
## 🗣 De quoi ça parle

Dans les années 80, on suit la vie de Philippe, un jeune homme introverti et qui se cherche. Il tient une radio pirate où il s’occupe de la partie technique laissant l’animation à son grand frère Jérôme, bien plus extraverti et sanguin. Entre les deux hommes se trouve Marianne, la petite amie de Jérôme, auprès de laquelle Philippe tente de se rapprocher. Mais la vie de Philippe bascule lorsqu’il apprend qu’il doit effectuer son service militaire à Berlin. Malgré l’éloignement, Philippe tentera de communiquer avec Marianne à travers une radio britannique en diffusant un message sonore très particulier.

## 🔎 Comment je l'ai découvert

A l’affiche dans le cinéma près de chez moi.

## 💭 Ce que j'en ai pensé

On ressent une atmosphère apaisante pendant le film. Cela est dû au caractère doux et timide de Philippe, très contrasté avec son frère, archétype du mâle alpha. 

J’ai beaucoup aimé certaines scènes où Philippe est un virtuose de l’enregistrement et des bandes magnétiques. Je vous laisse découvrir ces moments inattendus 😃

Pourtant, j’aurais apprécié plus d’interludes musicaux, apportant un peu de dynamisme au film qui manque parfois de vivacité. Néanmoins, pour terminer sur une note positive, on ressort du film avec un doux sentiment de bien-être qui fait du bien.

## 🎯 Qui l'apprécierait

Ceux qui font de la radio.

## 🔗 Œuvres reliées

Les films sur la jeunesse.