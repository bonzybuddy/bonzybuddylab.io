---
title: "La Fièvre de Petrov"
date: 2022-03-09T01:52:23+01:00
# maj: "08 Fév 2021"
draft: false
author : "Kirill Serebrennkinov"
principal_actors: "Semyon Serzin, Chulpan Khamatova, Vladislav Semiletkov"
genres:
  - Drame
  - Fantastique
release_year: "2021"
duree : "2h25"
cover: 
  img: https://m.media-amazon.com/images/M/MV5BYThiMzk0MDItMTYyMi00MjIwLTk2YzUtZTQ3YzkxMzY5YjY2XkEyXkFqcGdeQXVyMzA4MDA0Mjc@._V1_.jpg
  source: https://www.imdb.com/title/tt10380900/
#toc: false
---
## 🗣 De quoi ça parle

Petrov doit rentrer chez lui car il est très malade dû à une étrange fièvre qui sévit dans tout Moscou. Cependant, il se retrouve entraîné malgré lui dans un corbillard très alcoolisé d’où il réussit à s’échapper tant bien que mal. Arrivé chez lui il s’occupe de son fils alité qui délire lui aussi. Tout le monde tousse, tout le monde rêve éveillé, c’est un délire collectif. Des flashbacks de son enfance ressurgissent à tout moment et ce sont deux époques différentes qui se mettent en place. De son côté, sa femme est elle aussi prise de violentes pulsions aussi bien lors de son travail de bibliothécaire que lorsqu’elle s’occupe de son fils.

## 🔎 Comment je l'ai découvert

A travers la bande-annonce avant une séance.

## 💭 Ce que j'en ai pensé

Le film est complètement loufoque et il fait perdre la tête à l’image du personnage principal, fiévreux et délirant. La chronologie est décousue, l’imaginaire se mélange au réel, on ne sait plus qui l'on est ni où l’on est. Kirill Serebrennikov réussit à faire errer son spectateur entre différents mondes psychédéliques aux couleurs et sonorités ahurissantes. L’affiche du film résume assez bien cette virée enivrante qu’est la Fièvre de Petrov. 

Je retiens aussi un plan vertigineux qui m’a retourné le cerveau au-dessus d’une barre d’immeuble. 

Pour mieux (tenter de) comprendre cette review il ne vous reste plus qu’à aller voir le film. 

## 🎯 Qui l'apprécierait

Ceux qui aiment se perdre.

## 🔗 Œuvres reliées

Leto, du même réalisateur.