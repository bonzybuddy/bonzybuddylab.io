---
title: "Tralala"
date: 2021-10-14T15:52:25+02:00
# maj: "08-02-2021"
draft: false
author: "Arnaud et Jean-Marie Larrieu"
principal_actors: "Mathieu Amalric, Josiane Balasko, Mélanie Thierry"
genres:
  - Comédie Musicale
release_year: "2021"
duree: "2h"
cover:
  img: https://m.media-amazon.com/images/M/MV5BOWNkZjc2M2MtMmQ2MC00NWVjLTllNzItNGQ1NTQ3NTBmYjk2XkEyXkFqcGdeQXVyMTMwMDA2NjUx._V1_.jpg
  source: https://www.imdb.com/title/tt12591668/?ref_=nv_sr_srsg_0
---

_Cette review est extraite de mon [Top 10 des films d'octobre 2021]({{< ref "/films/top10.md#tralala" >}}_

Ce n'est pas par hasard si je place Tralala au début de cette liste. C'est sans doute le film que j'ai le plus apprécié parmi tous et je n'hésiterai pas à le classer comme mon film préféré de l'année. Tralala est pourtant une comédie musicale et je pensais ne pas aimer les comédies musicales où le rythme du film est interrompu par les chorégraphies et les chansons. Après avoir vu Tralala, je sais que je peux remettre en cause mon jugement sur les comédies musicales.

Le film suit un mendiant appelé Tralala qui joue de la guitare dans les rues de Paris pour gagner sa vie. Les premières scènes du films ne sont d'après moi pas les meilleures et il faut s'accrocher un peu pour rester dans le film. Tralala fait la rencontre d'une jeune fille devant la gare Montparnasse qu'il prend pour une apparition divine. Entre rêve et réalité et après un verre en terrasse, elle disparaît soudainement en laissant comme souvenir un briquet provenant de la ville de Lourdes. Tralala décide alors de s'y rendre pour la retrouver. Une fois sur place, en plus de retrouver sa muse, il va être embarqué au cœur d'une histoire de famille...

J'ai vraiment presque tout adoré du film. Premièrement la musique, qui mélange chanson française, disco, rock et même un peu de rap. J'ai grandement apprécié tous ces moments de chanson accompagnés de chorégraphies parfois éblouissantes. Les couleurs de certaines scènes sont incroyables, les acteurs sont attachants et la fin bien que prévisible est émouvante. C'est un film qui m'a rendu très heureux et joyeux en sortant de la salle, ce qui achève ma note plus que positive. Un sans-faute selon moi.


