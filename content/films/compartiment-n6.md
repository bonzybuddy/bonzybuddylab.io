---
title: "Compartiment n°6"
date: 2021-11-17T16:10:01+01:00
# maj: "08 Fév 2021"
draft: false
author: "Juho Kuosmanen"
principal_actors: Yuriy Borisov, Seidi Haarla, Dinara Drukarova
genres:
  - Drame
  - Voyage
  - Roman
release_year: "2021"
duree : "1h47"
cover: 
  img: https://m.media-amazon.com/images/M/MV5BYTU0ZjM5NzktZGFiOS00MzNmLWI3ZDAtYTE0MDQzNDBhNTM3XkEyXkFqcGdeQXVyMTI0MjI0NTc0._V1_.jpg
  source: https://www.imdb.com/title/tt10262648/?ref_=nv_sr_srsg_0
#toc: false
---

## 🗣 De quoi ça parle

La Russie dans les années 90. Une jeune étudiante en archéologie se rend à Mourmansk dans l'Arctique pour observer les pétroglyphes, des pierres millénaires gravées par d'anciens peuples. Pour aller de Moscou à Mourmansk elle voyage en train mais l'homme qui partage son compartiment ne semble pas de bonne compagnie. Ils vont peu à peu se découvrir et se lier d'amitié.

## 🔎 Comment je l'ai découvert

Grand Prix au festival de Cannes 2021, je l'ai découvert dans un programme de cinéma. 

## 💭 Ce que j'en ai pensé

J'ai trouvé le film représentatif de la Russie des années 1990 : froide et austère. Malgré l'absence de date clairement évoquée, on arrive à replacer l'époque grâce aux objets qu'utilisent les personnages comme le caméscope ou la cabine téléphonique.

Il n'y a pas beaucoup de personnages et on ne sait pas grand chose sur les deux protagonistes. Laura est étudiante finlandaise en archéologie et Ljoha est un russe qui part travailler dans une mine. C'est tout. Le scénario n'est pas compliqué, rien de prétentieux ou d'alambiqué. On ressent même de la poésie dans le caractère de la jeune femme qui regarde par la fenêtre du train ou qui filme des événements de la vie quotidienne avec son caméscope. La chanson "Voyage, Voyage" de Desireless accompagne le film du début à la fin. Quand Laura veut s'isoler du monde extérieur elle prend son Walkman et écoute ce titre. Elle est donc rêveuse mais aussi un peu mal à l'aise et cela se traduit avec toute la première partie du film tournée caméra à l'épaule. Ce choix de tournage permet évidemment aussi de recréer le mouvement ballottant du train mais finit par donner le tournis au spectateur. De plus, beaucoup de scènes sont tournées dans l'obscurité.

Heureusement, Yuriy Borisov qui interprète avec brio le personnage de Ljoha, permet de rattraper ces quelques points négatifs. Le personnage qu'il incarne fait pétiller le film et apporte des touches d'humour. J'ai aimé toutes les scènes dans lesquelles il apparaît. Il m'a donné envie de voir ses autres films.

Le film résume bien la phrase "ce n'est pas la destination qui compte mais le chemin emprunté". En effet, les pétroglyphes apparaissent comme le but de Laura qui a traversé tout le pays pour les observer. Au final, ces fameux pétroglyphes sont relégués au second plan et c'est une toute autre histoire qui s'est créée à partir de la rencontre avec Lhoja.

## 🎯 Qui l'apprécierait

Ceux qui apprécient les films sur la Russie ou qui se déroulent dans un train. 

## 🔗 Œuvre(s) reliée(s)

Le roman éponyme de Rosa Liksom paru en 2011 dont est adapté le film.
