---
title: "Madres Paralelas"
date: 2022-03-11T01:45:58+01:00
# maj: "08 Fév 2021"
draft: false
author: "Pedro Almodóvar"
principal_actors: "Penélope Cruz, Milena Smit, Israel Elejalde"
genres:
  - Drame
release_year: "2021"
duree: "2h03"
cover:
  img: https://m.media-amazon.com/images/M/MV5BM2Y2MWIzY2YtZDJiYi00ODM3LWE5NGYtNjlkYWE5ZmMxNTdmXkEyXkFqcGdeQXVyOTgxNDIzMTY@._V1_.jpg
  source: https://www.imdb.com/title/tt12618926/)
#toc: false
---

## 🗣 De quoi ça parle

Deux mères célibataires se rencontrent dans une maternité à Madrid. L’une (Janis) est photographe pour un magazine, l’autre est à peine adolescente. Chacune a donc des parcours et des âges très différents mais vont se retrouver liées malgré elles par un malencontreux évènement. Elles seront alors amenées à se revoir et tisser des liens assez forts. En même temps, le père de l’enfant de Janis refait surface car, étant anthropologue, il peut l’aider à ouvrir la fosse commune de son village natal afin de déterrer les cadavres dans le but de les rendre à leurs familles. Ces fosses communes sont le témoignage d’un passé lourd de conséquences laissées par le dictateur Franco lors de ses années de pouvoirs en Espagne. Janis est alors tiraillée entre deux défis, l’un consacré à son bébé et l’autre envers l’ouverture de la fosse commune.

## 🔎 Comment je l'ai découvert

A l’affiche du cinéma près de chez moi.

## 💭 Ce que j'en ai pensé

C’est la première fois que je voyais un film Almodóvar. J’ai donc naturellement été touché par son sens du décor raffiné et ses couleurs vives. Mais malgré cette joie en apparence, se cache des émotions bien plus sombres qui se lisent sur le visage des acteurs. Ils ne sont qu’à demi heureux et quelque chose manque à leur bonheur. Ce manque à combler va être alors la quête de tout le film.

Pourtant, ce n’est pas ce sentiment de tristesse que j’ai eu pendant la séance, même si les moments mélancoliques ou dramatiques m’ont bien sûr touché. Ce n’est qu’après la séance que j’ai compris tous les enjeux que le film souhaitait soulever et raconter.

La musique signée Alberto Iglesias s’accorde parfaitement avec le thème et les sentiments du film et reste imprégnée dans la tête même après la séance.

Encore une fois, c’est un film qui m’a donné envie de découvrir la filmographie du réalisateur.

## 🎯 Qui l'apprécierait

Je pense que tout le monde l’apprécierait car il ne vise pas un public en particulier et l’état d’esprit dans lequel il nous transporte est agréable.

## 🔗 Œuvres reliées

Les autres films de Pedro Almodóvar.
