---
title: "The Green Hornet"
date: 2021-12-10T11:06:27+01:00
# maj: "08 Fév 2021"
draft: false
author: "Michel Gondry"
principal_actors: Seth Rogen, Cameron Diaz, Jay Chou
genres:
  - Action
  - Comédie
release_year: "2011"
duree: "1h59"
cover:
  img: https://m.media-amazon.com/images/M/MV5BYjlhN2U3OWUtNThiNi00ZWRhLWFlZDktOTcyMDk0NmM1OTA4XkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_.jpg
  source: https://www.imdb.com/title/tt0990407/?ref_=nv_sr_srsg_0
#toc: false
---

## 🗣 De quoi ça parle

Britt est un fêtard impulsif et égoïste qui doit reprendre le journal de son père venant de mourir subitement. Le journal menace de faire faillite avec ce nouveau rédacteur en chef. Britt découvre alors que l'ancien chauffeur de son père, Kato, est aussi un mécanicien et inventeur de génie. Contre toute attente et malgré eux, Britt et Kato commencent à se lier d'amitié et ont l'idée de former un duo de super héros rivalisant avec les gangs de Los Angeles. Forts de cette double identité, cette idée leur permet d'avoir l'exclusivité des aventures de The Green Hornet dans leur journal. Différents personnages vont alors intervenir pour essayer de faire tomber ce nouveau super héros et ruiner le journal.

## 🔎 Comment je l'ai découvert

Ma mère me l'a conseillé.

## 💭 Ce que j'en ai pensé

J'ai adoré ce film, c'est mon coup de coeur de l'année 2011 !

L'ambiance qui se dégage du film m'a énormément touché. La musique signée James Newton Howard est superbe. L'accumulation de gadgets est impressionnante d'ingéniosité et donne une dynamique au film. Le rôle du méchant est interprété avec brio par Christoph Waltz qui fait assurément partie de mes acteurs préférés. Les touches d'humour apportées par Seth Rogen pimentent le film et lui confère son côté comédie. Les scènes d'action sont également très bien exécutées et ne démentent pas. De plus, la fin est riche en rebondissements ce qui est un pur plaisir.

Cette dualité action/comédie ne manque pas de créativité sans tomber dans le piège de la comédie pathétique.

## 🎯 Qui l'apprécierait

Les amateurs de films d'actions avec de l'humour. Ceux qui aiment les gadgets dans le style de James Bond et les belles voitures.

## 📽 Films reliés

Les autres films de Michel Gondry et tous les films de super héros un peu comiques.
