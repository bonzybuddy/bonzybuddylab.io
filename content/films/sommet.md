---
title: "Le Sommet des Dieux"
date: 2021-10-09T15:51:49+02:00
# maj: "08-02-2021"
draft: false
author: "Patrick Imbert"
genres:
  - Animation
release_year: "2021"
duree: "1h30"
cover:
  img: https://m.media-amazon.com/images/M/MV5BOTM5ODFiOGUtYTYwZC00MDM3LWJkMjItMDQ5YmU5ZmQxZDJhXkEyXkFqcGdeQXVyMjA1MDAxMDU@._V1_.jpg
  source: https://www.imdb.com/title/tt7014378/?ref_=nv_sr_srsg_0
---

_Cette review est extraite de mon [Top 10 des films d'octobre 2021]({{< ref "/films/top10.md#le-sommet-des-dieux" >}})_

Le Sommet des Dieux est une merveille de l'animation. A cause de l'animation justement, j'ai hésité avant d'aller le voir. Je serais vraiment passé à côté de quelque chose. Je pense que le manga aussi vaut le détour, il est notamment très bien documenté sur l'alpinisme et sa technique. L'histoire dans le manga est aussi plus développée que dans le film qui a par ailleurs nécessité 8 ans de travail !

Le film est donc adapté du manga de Jirō Taniguchi, lui-même inspiré du roman de Baku Yumemakura sur l'alpinisme japonais dans les années 90. L'histoire est racontée et vue à travers les yeux d'un reporter photo de montagne qui suit les expéditions durant leurs ascensions et immortalise leur succès avant de vendre ses clichés. Un élément déclencheur le met sur la trace d'Habu Jôji un alpiniste renommé qui a disparu de la scène internationale après d'illustres ascensions sur les faces les plus difficiles du monde. Ce mystérieux Habu détiendrait l'appareil photo original de George Mallory, premier alpiniste qui tenta d'atteindre le sommet de l'Everest en 1924 avec son compatriote Andrew Irvine, 30 ans avant la première réussite confirmée. Ils ont été aperçu pour la dernière fois à 245m du sommet avant de disparaître à jamais, sans que personne ne sache s'ils ont réussi. Le développement de la pellicule de l'appareil de Mallory permettrait alors de connaître la vérité...

Je trouve que le scénario est haletant et donne vraiment envie de connaître la suite. Cette intrigue autour de l'appareil photo mais aussi d'Habu est très bien ficelée. La première chose a relever concernant ce film est la beauté du dessin. En effet, le coup de crayon est splendide, c'est un plaisir pour les yeux. Les couleurs sont également très travaillées, avec cette dominante blanche de la neige népalaise. La musique d'Amine Bouhafa atteint elle aussi des sommets et nous transporte dans l'action des personnages. A voir absolument.
