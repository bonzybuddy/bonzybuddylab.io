---
title: "Illusions perdues"
date: 2021-10-21T15:52:54+02:00
# maj: "08-02-2021"
draft: false
author: "Xavier Giannoli"
principal_actors: "Benjamin Voisin, Vincent Lacoste, Cécile de France"
genres:
  - Histoire
  - Roman
  - Drame
release_year: "2020"
duree: "2h"
cover:
  img: https://m.media-amazon.com/images/M/MV5BNzNlMDZkMWMtYjY4Ny00YTJlLTgzNGItNGFhMDQzNjdjMmRjXkEyXkFqcGdeQXVyMTAyMjQ3NzQ1._V1_.jpg
  source: https://www.imdb.com/title/tt10505316/?ref_=nv_sr_srsg_0
---

_Cette review est extraite de mon [Top 10 des films d'octobre 2021]({{< ref "/films/top10.md#illusions-perdues" >}})_

Illusions perdues est le titre du roman de Balzac dont est adapté ce film de Xavier Giannoli. N'ayant pas lu le roman je n'ai aucun moyen de comparaison avec le film. C'est mon premier film de ce réalisateur dont j'ai entendu du bien.

Lucien de Rubempré est un jeune poète qui travaille dans une imprimerie de province. Grâce à la marquise de ... qui joue le rôle de mécène pour les petits artistes, il monte à Paris dans le but de faire fortune en publiant ses poèmes. Malheureusement la vie parisienne est dure et cruelle et Lucien ne connaît pas les coutumes de cette nouvelle vie. Il finit par se faire engager dans un journal libéraliste.

Lucien de Rubempré (qui s'appelle en réalité Chardon) est un personnage qui n'attire pas l'empathie. Il est rancunier et sans scrupules. Son talent est tout compte fait limité puisqu'il ne publiera jamais aucun poème hormis un petit recueil médiocre. On est donc plongé dans le monde du journalisme, du théâtre, de la littérature et du combat entre l'ancienne aristocratie sur le déclin et la nouvelle bourgeoisie riche et puissante. Le film est relativement précurseur avec sa vision des journaux corrompus et mensongers qui écrivent n'importe quoi pourvu qu'ils vendent. On peut donc y voir le début de notre société capitaliste actuelle avec le pouvoir de l'argent. Pourtant, je n'ai pas vraiment aimé le film et les acteurs ne m'ont pas convaincus. A aucun moment je n'ai ressenti de l'empathie un personnage. Je me suis consolé en me disant qu'ils étaient écrit de telle manière par Balzac mais je n'en ai aucune idée. La musique ne m'a pas particulièrement marqué.


