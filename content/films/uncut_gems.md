---
title: "Uncut Gems"
date: 2022-03-14T01:40:00+01:00
# maj: "08 Fév 2021"
draft: false
author: "Josh et Benny Safdie"
principal_actors: "Adam Sandler, Julia Fox, Idina Menzel"
genres:
  - Drame
  - Thriller
release_year: "2019"
duree: "2h15"
cover:
  img: https://m.media-amazon.com/images/M/MV5BZDhkMjUyYjItYWVkYi00YTM5LWE4MGEtY2FlMjA3OThlYmZhXkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg
  source: https://www.imdb.com/title/tt5727208/
#toc: false
---

## 🗣 De quoi ça parle

Howard, un bijoutier New-Yorkais pense avoir découvert une nouvelle roche incrustée de pierres précieuses qu’il compte revendre aux enchères à prix d’or afin de rembourser ses nombreuses dettes et de payer son divorce. Parmi ses clients, Kevin Garnett _himself_ le célèbre basketteur des Celtics croit voir en cette pierre un talisman précieux qui l’aiderait à gagner ses matchs. Mais le temps presse et Howard est poursuivi sans relâche par ses débiteurs qui commencent à perdre patience. Heureusement, Howard peut compter sur sa maîtresse Julia, prête à tout pour l’aider.

## 🔎 Comment je l'ai découvert

[Sacha](https://hjkl.it) me l’a fortement recommandé.

## 💭 Ce que j'en ai pensé

Le film est complètement frénétique avec un Adam Sandler qui n’arrête jamais de parler, de bouger, de crier et de téléphoner en même temps. Je pense que cette frénésie est liée à la vie new-yorkaise, très éprouvante et que les réalisateurs ont probablement voulu faire transparaitre. Malgré son étiquette de _looser_ le personnage d’Howard est très attachant et on se trouve happé dans son univers et ses magouilles. Ses côtés comiques sont sublimés par un Adam Sandler dans son élément.

Julia Fox, découverte grâce à ce film est formidable et remplit son rôle à merveille. Son personnage qui paraît accessoire au début, se révèle d’une importance cruciale par la suite.

La musique originale est superbe et je ne me lasse pas d’écouter la BO. Cette musique un peu mystique voire psychédélique mêlée aux couleurs flashy bleues et roses de la bijouterie où se déroulent de nombreuses scènes rendent une atmosphère très particulière.

J’ai été conquis par ces scènes tournées dans le quartier de Diamond District à Manhattan qui sont assez inédites car peu de réalisateurs y ont déjà tourné et l’on y voit un New-York foisonnant, fourmillant et débordant mais aussi et surtout très avide et impitoyable.

Là où le film rejoint ceux de Scorsese, c’est dans cet univers de diaspora/mafia (ici de bijoutiers) d’où le héros ne pourra pas s’échapper.

Uncut Gems m’a donc donné envie de découvrir la filmographie des Safdie et même de revoir le film.

## 🎯 Qui l'apprécierait

Les amateurs des dialogues de Martin Scorsese et de New-York.

## 🔗 Œuvres reliées

Les autres films des frères Safdie et de Scorsese. Les vlogs de [Casey Neistat](https://www.youtube.com/user/caseyneistat) pour vivre New-York au plus près.
