---
title: "Belfast"
date: 2022-03-17T08:23:56+01:00
# maj: "08 Fév 2021"
draft: false
author: "Kenneth Branagh"
principal_actors: "Jude Hill, Lewis McAskie, Caitriona Balfe"
genres:
  - Drame
release_year: "2022"
duree: "1h39"
cover:
  img: https://m.media-amazon.com/images/M/MV5BODMwYTYyY2ItOWQ5Yi00OTI1LTllYTQtYTdlNWM4YzJhYTM0XkEyXkFqcGdeQXVyMTA2MDU0NjM5._V1_.jpg
  source: https://www.imdb.com/title/tt12789558/
#toc: false
---

## 🗣 De quoi ça parle

L’enfance du jeune Buddy et de sa famille à Belfast dans la fin des années 1960. La haine des protestants contre les catholiques est au plus haut, de violents affrontements ont lieu au cœur de Belfast la capitale de l’Irlande du Nord. Pour éviter ces émeutes, le père pousse sa famille à partir s’installer à Londres où il travaille comme menuisier. Mais ils hésitent, car appréhendent le jugement des anglais envers des irlandais protestants. C’est dans ce climat de tensions que grandit Buddy, qui n’est autre que l’incarnation du jeune Kenneth Branagh. Le film est donc en grande partie autobiographique.

## 🔎 Comment je l'ai découvert

Dans le programme de ciné.

## 💭 Ce que j'en ai pensé

J’ai franchement adoré Belfast. Le noir et blanc plutôt en vogue en ce moment (The French Dispatch, Les Olympiades, Roma) joue ici son rôle d’époque et ajoute une dimension tragique au film. Cependant, la couleur, et quelle couleur !, est présente dans les scènes d’intro et de fin qui montrent le Belfast actuel, à la manière d’un clip touristique avec de magnifiques plans de drone du paysage irlandais.

Les effets spéciaux associés à quelques ralentis sont très appréciables et accentuent l’aspect dramatique des heurts.

Tous les plans sont incroyables et méritent de s’y attarder pour les contempler et les analyser. J’ai essayé pendant la séance d’observer le cadrage, la lumière, les mouvements de caméra etc. et il se trouve que chaque plan est significatif ; minutieusement recherché et travaillé. J’aime beaucoup cette approche technique qui permet, en plus de mieux comprendre le film, d’apprécier le travail de fond du réalisateur et du directeur de la photographie et de garder en mémoire certaines manières de traiter une idée.

La musique composée par Van Morisson (né à Belfast) est superbe et semble toujours en décalage avec l’émotion de la scène, des musiques joyeuses accompagnent des moments tristes, comme si malgré la gravité des événements, Kenneth Branagh préférait voir le bon côté plutôt que l’ombre.

Le casting est formidable. Les acteurs sont tous dans leur élément, la plupart étant irlandais. Le jeune Jude Hill à seulement 9 ans est vraiment bluffant et toutes les émotions se lisent sur son visage de manière très pure. Les grands-parents (Judi Dench et Ciarán Hinds) imposent leur sagesse et leur vision de la vie.

## 🎯 Qui l'apprécierait

Tout le monde, comme Madres Paralelas, je pense qu’il plairait à tous.

## 🔗 Œuvres reliées

Hormis d’autres films récents en noir et blanc, les films autobiographiques.
