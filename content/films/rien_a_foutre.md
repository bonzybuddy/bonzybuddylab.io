---
title: "Rien à foutre"
date: 2022-03-16T10:45:00+01:00
# maj: "08 Fév 2021"
draft: false
author: "Emmanuel Barre et Julie Lecoustre"
principal_actors: "Adèle Exarchopoulos, Alexandre Perrier, Mara Taquin"
genres:
  - Drame
  - Comédie

release_year: "2021"
duree: "1h55"
cover:
  img: https://m.media-amazon.com/images/M/MV5BODU1NzA1MTQtZTkxMS00M2M0LWJlZWQtNThmNzQyNWMxOTczXkEyXkFqcGdeQXVyODY0MDQ3NTI@._V1_.jpg
  source: https://www.imdb.com/title/tt13482806/
#toc: false
---

## 🗣 De quoi ça parle

Cassandre est une jeune hôtesse de l’air travaillant pour une compagnie low-cost européenne. Elle rêve de changer de compagnie mais n’ose pas postuler ailleurs, se contentant de ce qu’elle a et d’accepter tout ce que lui demande son supérieur. On lui demande de travailler à Noël, elle dit oui, de passer une formation pour devenir cheffe de cabine et éviter de rompre son contrat, elle accepte alors que ça ne lui plaît pas vraiment. Elle est basée à Lanzarote et rentre presque tous les soirs ce qui lui permet de souvent faire la fête dans les clubs branchés de cette station balnéaire. Elle ne s’attache à personne et n’en ressent pas l’envie, elle préfère vivre au jour le jour. Le film se concentre entièrement sur la vie de Cassandre dans tous ses moments, la plupart du temps solitaire et toujours de passage.

## 🔎 Comment je l'ai découvert

Dans le programme de cinéma de la semaine.

## 💭 Ce que j'en ai pensé

Le film montre l’ambiance de travail d’une compagnie aérienne sans scrupules envers ses employés. Le moindre faux pas est réprimandé et il faut toujours obtenir de meilleurs résultats de vente ou de satisfaction clientèle malgré tout le mal que les hôtesses se donnent. La scène des grévistes est intéressante car elle permet de se rendre compte que la plupart des hôtesses se disent satisfaites de leurs conditions et ne prennent pas vraiment le temps de trouver ce qui pourrait être amélioré.

Au vu du titre je m’attendais à ce que l’héroïne quitte rapidement son travail pour une autre vie mais elle se sous-estime aux yeux de meilleures compagnies jusqu’à ce qu’une ancienne collègue lui propose une autre carrière.

J’ai adoré certaines scènes/plans, assez recherchés comme celui correspondant à l’affiche, celle magnifique et si juste où Cassandre et David, un steward, rentrent de soirée complètements bourrés et se racontent leur vie. Ou encore cette scène où l’on ne distingue que le bout rougeoyant des cigarettes des deux sœurs avec par moments l’éclat de lumière sur leurs visages provoqué par la flamme du briquet. Les plans contemplatifs, notamment dans la deuxième partie du film, tranchent avec l’ambiance clubbing des fêtes espagnoles.

Enfin, Adèle Exarchopoulos avec son air désinvolte et détaché remplit à merveille son rôle d’hôtesse mais aussi de sœur. C’est un film profond sur le développement de son personnage principal et de ses émotions.

## 🔗 Œuvres reliées

Low Cost avec Jean-Paul Rouve ? Non, je ne vois pas d’autres films.
