---
title: "Bullet Train"
date: 2023-03-26T16:39:54+02:00
# maj: "08 Fév 2021"
draft: false
author: "David Leitch"
principal_actors: "Brad Pitt, Joey King, Aaron Taylor-Johnson"
genres:
  - Action
  - Comédie
  - Thriller
release_year: "2022"
duree: "2h06"
cover:
  img: https://m.media-amazon.com/images/M/MV5BMDU2ZmM2OTYtNzIxYy00NjM5LTliNGQtN2JmOWQzYTBmZWUzXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg
  source: https://www.imdb.com/title/tt12593682/?ref_=nv_sr_srsg_0
#toc: false
---

Ce film me faisait de l’oeil depuis son annonce. En naviguant sur [IMDB](https://www.imdb.com) à la recherche d’infos sur mes publi, je suis tombé sur la bande-annonce de Bullet Train. Action, comédie, haut en couleurs et avec Brad Pitt, difficile de faire plus aguicheur. Après [Fight Club]({{< ref "../films/fight_club/" >}}), Once Upon a Time in Hollywood et plus récemment [Babylon]({{< ref "../films/babylon/" >}}), je continue à m’intéresser à la filmographie de Brad Pitt.

Pour en revenir à Bullet Train, attendez-vous à de l’action, beaucoup d’action. Niveau colorimétrie idem, on s’en prend plein la figure. Filmé quasi exclusivement à bord du Shinkansen, ce fameux train ultra-rapide qui relie les métropoles japonaises, tout défile à toute allure. Mélangez-y des tueurs à gages impitoyables, une criminelle aussi manipulatrice que séduisante, des otages et un agent secret malchanceux, le tout agrémenté d’histoires de familles et vous obtenez un cocktail pour le moins explosif !

Le scénario est simple; le héros doit récupérer une mallette remplie de cash à bord d’un train. L’opération, simple à première vue, se complique lorsque le héros découvre que cette mallette est détenue par deux tueurs à gages. Eux-mêmes doivent livrer un otage au plus grand criminel du monde. Tandis qu’un autre personnage tente de venger son fils et se trouve lui aussi otage d’une jeune femme pernicieuse prête à tout pour arriver à ses fins.

Vous l’aurez compris, chacun joue sa peau mais tous les destins se retrouvent liés à mesure que le film avance. Petit à petit, la tension monte, le _bullet train_ roule vite, telle une machine infernale courant à sa perte… On y retrouve un goût de Snowpiercer (Le Transperceneige, excellent film au passage, j’y reviendrai prochainement après l’avoir revu) en raison de son déroulement dans un train lancé à toute vitesse et pour l’action qui y règne. Mais la comparaison s’arrête là.

Si le film n’est pas un chef-d’oeuvre, c’en n’est pas moins un bon divertissement où l’on ne s’ennuie pas, car les rebondissements n’en finissent pas. Les scènes de combat sont légion et la fin est épique. Attention aux surenchères d’effets spéciaux, on est dans la démesure du blockbuster américain par excellence. Comédie oblige, une dose d’humour est également au rendez-vous avec des comiques de situation amusants.

A voir sur grand écran pour une immersion totale 😉
