---
title: "Tre Piani"
date: 2022-03-10T01:45:11+01:00
# maj: "08 Fév 2021"
draft: false
author : "Nanni Moretti"
principal_actors: "Riccardo Scamarcio, Margherita Buy, Alba Rohrwacher"
genres:
  - Drame
release_year: "2021"
duree : "1h59"
cover: 
  img: https://m.media-amazon.com/images/M/MV5BM2Q0ZmU2MDgtNDlhNS00MWY1LTkwZjgtNWU2NTdhY2Y2NGJhXkEyXkFqcGdeQXVyODA0MjgyNzM@._V1_.jpg
  source: https://www.imdb.com/title/tt9110904/
#toc: false
---
## 🗣 De quoi ça parle

Les vies respectives de quatre familles habitant le même immeuble à Rome. Chacune de ces familles se connaissent mais certains évènements dramatiques viennent bousculer leurs vies. Plusieurs histoires sont alors imbriquées les unes dans les autres, qui se croisent puis s’éloignent. Chacun se retrouve rattrapé par les fantômes du passé ce qui les pousse à prendre des résolutions.

## 🔎 Comment je l'ai découvert

A l’affiche dans mon cinéma habituel.

## 💭 Ce que j'en ai pensé

On retrouve toute l’équipe d’acteurs fétiches de Moretti. Ils sont toujours aussi bons alors pourquoi s’en priver ? Mention spéciale à Riccardo Scamarcio qui est superbe. 

Depuis quelques années les films du réalisateur sont très dramatiques et celui-ci n’échappe pas à la règle avec un début d’intrigue presque malaisant, plein de non-dits. Je n’ai pas donc pas vraiment aimé le début de Tre Piani à cause de cette gêne. Heureusement, le film ne continue pas dans cette optique et se diversifie à travers la vie de chacun des personnages principaux. 

Le film n’est pas joyeux car toutes les histoires sont sombres mais chacun tente de réparer ou de fuir les erreurs du passé à sa manière. Finalement, Tre Piani est très touchant et c’est ce qui fait la force de Nanni Moretti, aller directement et sensiblement au coeur de ses spectateurs.

## 🎯 Qui l'apprécierait

Les amateurs de films italiens et de drame.

## 🔗 Œuvres reliées

Tous les autres films de Nanni Moretti.