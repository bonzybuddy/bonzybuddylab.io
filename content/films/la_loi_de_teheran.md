---
title: "La Loi de Téhéran"
date: 2022-03-12T01:52:36+01:00
# maj: "08 Fév 2021"
draft: false
author: "Saeed Roustayi"
principal_actors: "Payman Maadi, Navid Mohammadzadeh, Parinaz Izadyar"
genres:
  - Action
  - Thriller
release_year: "2021"
duree: "2h11"
cover:
  img: https://m.media-amazon.com/images/M/MV5BOTA0ODc5YjEtY2EzYy00ZDFhLWEzYmItZDgwNDhiMzE4Y2U0XkEyXkFqcGdeQXVyODIyOTEyMzY@._V1_.jpg
  source: https://www.imdb.com/title/tt9817070/
#toc: false
---

## 🗣 De quoi ça parle

La poursuite par la police des consommateurs et trafiquants de drogue en Iran. L’inspecteur et son équipe tentent de démanteler le plus gros réseau de drogue de Téhéran en attrapant le gros poisson, mais celui-ci est introuvable. La traque des forces de l’ordre est frénétique car la possession d’un maximum de 6,5 grammes de drogue est suffisant pour être passible de peine de mort. Leur enquête vont les mener assez loin dans le milieu avant de s’achever sur des conséquences dramatiques.

## 🔎 Comment je l'ai découvert

Grâce à la bande-annonce.

## 💭 Ce que j'en ai pensé

Le film est une belle claque. J’ai beaucoup aimé. Il est intense et prenant. Il dénonce surtout le régime autoritaire du pays et oppose le point de vue “officiel” contre celui des trafiquants et des consommateurs. Certaines scènes sont choquantes de réalité sur les conditions de vie et de traitement des pauvres (et des moins pauvres) en Iran. Chacun tente en réalité de survivre et chacun choisit alors sa voix: le chasseur ou le chassé avec les conséquences qu’il faut être prêt à assumer dans un cas.

Le film interroge, s’agite et détonne au sens propre du terme. Je ne peux que vous le recommander, ce qui permet aussi d’accroitre la notoriété de tels réalisateurs en France et plus globalement en Europe, où leurs films sont appréciés à leur plus juste valeur.

## 🎯 Qui l'apprécierait

Les amateurs de films d’action et de thriller. Ceux qui supportent le cinéma indépendant du Moyen-Orient.

## 🔗 Œuvres reliées

Le Diable n’existe pas de Mohammad Rasoulof.
