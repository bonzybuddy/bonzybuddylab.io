---
title: "Titane"
date: 2021-10-07T15:38:39+02:00
# maj: "08-02-2021"
draft: false
author: "Julia Ducournau"
principal_actors: "Agathe Rousselle, Vincent Lindon"
genres:
  - Thriller
  - Drame
release_year: "2021"
duree: "1h50"
cover:
  img: https://m.media-amazon.com/images/M/MV5BNGJlMTVlYWQtYmM5OC00MDgwLTk3NzAtMGViY2VjOGU0YjlkXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg
  source: https://www.imdb.com/title/tt10944760/?ref_=nv_sr_srsg_0
---

_Cette review est extraite de mon [Top 10 des films d'octobre 2021]({{< ref "/films/top10.md#titane" >}})_

Je voulais voir Titane car il s'agit du lauréat de la Palme d'or 2021. Je suis donc allé le voir sans chercher à savoir si le thème me plairait ; j'étais persuadé qu'une palme d'or valait forcément la peine. J'ai (re)découvert le genre ultra-violent et noir, à la limite du fantastique que l'on retrouve dans [_Under the Skin_](https://www.imdb.com/title/tt1441395/?ref_=nv_sr_srsg_0) avec Scarlett Johannson.

Après un accident de voiture, Alexia subit une opération de la tête où les chirurgiens lui implantent une prothèse en titane. Quelques années plus tard, Alexia travaille en tant que danseuse dans un car show. Mais poussée par des pulsions, elle assassine plusieurs personnes avant d'être recherchée par toute la police de Marseille. Elle quitte alors son domicile familial et essaie de prendre l'apparence d'un jeune garçon disparu depuis 10 ans afin d'être adoptée par un nouveau père. Ce dernier est capitaine dans une caserne de pompier et reconnaît en Alexia son fils disparu, Adrien. Alexia/Adrien est recueilli et se mure dans un mutisme et les autres pompiers de la caserne ont du mal à l'accepter.

Une personne qui prend la place d'une autre disparue depuis des années, on retrouve ce que l'on avait dans Tralala qui n'a pourtant absolument rien à voir avec Titane. Cela démontre une fois de plus qu'une idée peut être travaillée de manière complètement opposée. Les acteurs sont formidables et malgré le peu de dialogues, beaucoup d'émotions passent par le langage non verbal. J'ai beaucoup aimé toutes les scènes de danse et de fête notamment celle au début dans ce car show et dans la caserne des pompiers. Les couleurs parfois excentriques et flashy, l'ambiance et la musique présentes m'ont happé et vraiment fait aimé ces quelques scènes. Dommage que je ne puisse pas en dire autant des autres où la violence est très présente et qui m'a heurté. Comme dans Drive avec Ryan Gosling, j'ai du mal avec les scènes ultra-violentes. Je trouve qu'il faut quand-même s'accrocher par moments pour ne pas fermer les yeux et les bruitages n'aident pas. J'ai donc un avis assez mitigé.
