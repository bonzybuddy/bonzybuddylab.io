---
title: "Spider-Man : New Generation"
date: 2021-04-25T21:02:01+02:00
# maj: "08-02-2021"
draft: false
author: "Peter Ramsey et Bob Persichetti"
genres:
  - Action
  - Animation
  - Aventure
release_year: "2018"
duree: "1h57"
cover:
  img: https://m.media-amazon.com/images/M/MV5BMjMwNDkxMTgzOF5BMl5BanBnXkFtZTgwNTkwNTQ3NjM@._V1_.jpg
  source: https://www.imdb.com/title/tt4633694/?ref_=nv_sr_srsg_0
---

## 🗣 De quoi ça parle

Miles, un adolescent new-yorkais, se fait piquer par une araignée radioactive et obtient alors les mêmes pouvoirs que Spider-Man. Quelques jours plus tard, alors qu'il s'interroge sur ce qui lui arrive, il est témoin d'une tentative d'ouverture d'un portail spatio-temporel provoquée par le Caïd (Kingpin) dans le but de ressusciter sa famille défunte. L'expérience tourne mal et des Spider-Men d'autres époques se retrouvent alors ensemble. Ils s'unissent afin de lutter contre le Caïd et pour que chacun puisse retourner dans sa dimension respective.

## 🔎 Comment je l'ai découvert

[Sacha](https://hjkl.it) me l'a conseillé

## 💭 Ce que j'en ai pensé

Ce film est une pépite d'animation et de graphismes pop flamboyants. Les couleurs de chaque plan sont très travaillées et j'ai aussi adoré la bande originale aux airs de hip-hop et de R'n'B. Seule l'histoire m'a parue un peu décousue, elle se retrouve reléguée au second plan tant l'animation et les dessins sont mis en valeurs. On y retrouve aussi un mix entre strips de comics et film d'animation. On est tenu en haleine durant les deux heures et on s'en prend plein les yeux ! Je ne peux que vous le recommander !

## 🎯 Qui l'apprécierait

Les amateurs de comics, d'action et de l'univers de Spider-Man.
