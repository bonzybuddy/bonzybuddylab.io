---
title: "Memoria"
date: 2021-12-15T10:40:43+01:00
# maj: "08 Fév 2021"
draft: false
author: Apichatpong Weerasethakul
principal_actors: Tilda Swinton, Elkin Díaz, Jeanne Balibar
genres:
  - Drame
  - Fantastique
release_year: "2021"
duree: "2h16"
cover:
  img: https://m.media-amazon.com/images/M/MV5BYTIxZDAwZGYtMDNlMS00YjNmLTk4OTItMTc1N2QxNGRiMmEzXkEyXkFqcGdeQXVyMTEyMjM2NDc2._V1_.jpg
  source: https://www.imdb.com/title/tt8399288/?ref_=nm_flmg_act_10
#toc: false
---

## 🗣 De quoi ça parle

Difficile de résumer l'histoire. Jugez par vous-même : Une femme insomniaque entend pendant une nuit un bruit sourd et effrayant. Dès lors, elle cherche la signification de ce bruit.

Honnêtement, c'est tout. Je ne vois pas quoi dire de plus. Le film se passe d'abord à Bogotá puis dans un village niché au coeur de la forêt amazonienne sans que l'on sache plus de détails. Avouez que le scénario est loufoque. Je lui ai d'ailleurs associé le tag de [Fantastique]({{< ref "../genres/fantastique/" >}}) car il y a quelque chose de surnaturel dans ce film.

## 🔎 Comment je l'ai découvert

Dans un programme de cinéma. Prix du jury au festival de Cannes 2021.

## 💭 Ce que j'en ai pensé

J'ai vraiment bien aimé le film même si je ne cache pas que je n'ai pas compris l'histoire. Les entourloupes de scénario de Christopher Nolan sont peu de chose comparé à Memoria. Ici, place au film d'auteur qui se déroule dans une quiétude presque parfaite. La quasi totalité des plans sont fixes et durent de (longues) minutes pendant lesquels il n'y a pas forcément d'action. Je parle d'action au sens mouvement des personnages. Je n'ai noté qu'une seule scène caméra à l'épaule qui dénote singulièrement avec le reste du film et je n'ai d'ailleurs pas compris ce choix de la part du réalisateur.

Il n'y a qu'une dizaine de personnages et les dialogues mélangent espagnol et anglais ce qui accentue l'originalité du film. Le reste des personnages se compose de figurants qui, à mon sens, font presque partie du film tellement ils sont nombreux et présents dans la première partie. Ils font vivre cette ville de Bogotá —capitale de la Colombie— et donnent envie de s'y rendre. Dans la seconde partie, celle qui se passe dans le village amazonien loin de l'effervescence de la ville, on se retrouve plongé dans un havre de paix et de verdure qui consolide le sentiment de tranquillité que j'ai pu ressentir tout au long du film. L'atmosphère qui s'en dégage est particulièrement reposante.

Concernant la musique, car vous savez que c'est un élément qui me tient à coeur dans les films, celui-ci comporte uniquement deux interludes musicaux. Ils sont malgré tout superbes et comme les plans durent, la musique dure avec. J'ai personnellement beaucoup aimé la scène qui se déroule dans le studio d'enregistrement où l'on peut admirer l'énorme console de mixage et le matériel utilisé. Le film ne pâtit pourtant pas du manque de musique car tout repose dans son silence. D'ailleurs, vu qu'il ne passe littéralement rien, quelle meilleure musique que le silence pour accompagner ce "rien" 😉

## 🎯 Qui l'apprécierait

Memoria est vraiment un film surprenant et je pense que les avis seront clairement mitigés entre ceux qui vont l'apprécier et ceux qui vont le détester. La preuve : sur les six personnes présentes dans la salle, deux sont parties au bout d'une heure et une autre m'a demandé ce que j'en avais pensé à la fin.

En tous cas, ceux qui aiment la nature, le calme et les films lents avec peu de dialogues sont servis et devraient sûrement beaucoup aimer Memoria. Pour tous les autres qui préfèrent les [jump-cut](https://www.youtube.com/watch?v=Fna75UW84qY) et l'action passez votre chemin.

## 🔗 Œuvres reliées

Les autres films de même réalisateur notamment _[Oncle Boonmee](https://www.imdb.com/title/tt1588895/?ref_=nv\*sr\*srsg*0)_, Palme d'Or en 2010. Les films lents et calmes ; je répète ce point mais il caractérise tellement le film que vous devriez en avoir bien conscience avant de le voir.
