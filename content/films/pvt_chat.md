---
title: "PVT Chat"
date: 2022-03-14T11:25:00+01:00
# maj: "08 Fév 2021"
draft: false
author: "Ben Hozie"
principal_actors: "Peter Vack, Julia Fox, Keith Poulson"
genres:
  - Drame
release_year: "2020"
duree: "1h26"
cover:
  img: https://m.media-amazon.com/images/M/MV5BOGY0YmVlOTYtMmYyMS00ZGY0LTk2Y2MtNDg2YmViNTkzMzMwXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg
  source: https://www.imdb.com/title/tt8110004/
#toc: false
---

## 🗣 De quoi ça parle

A New-York, Jack est un jeune parieur en ligne qui fréquente régulièrement des sites de camgirls. Il se retrouve très attiré par l’une d’entre elles, Scarlett, une dominatrix habitant à San Francisco. Afin de développer leur relation, Jack s’invente une vie auprès d’elle, lui faisant croire qu’il travaille dans la recherche informatique. Tout va basculer lorsqu’un soir il croit l’apercevoir dans un magasin de Chinatown à New-York.

## 🔎 Comment je l'ai découvert

A partir de la filmographie de Julia Fox.

## 💭 Ce que j'en ai pensé

Je ne m’attendais pas à un film de cette qualité. Le réalisateur, leader du groupe BODEGA, porte néanmoins très bien cette double casquette guitariste/réalisteur. Malgré un scénario relativement classique, on est pris dans l’intrigue et le dénouement nous tient en haleine.

Le cadre est presque toujours de travers, comme pour suggérer un malaise des personnages et une tragédie à venir. Les scènes extérieures sont toutes tournées de nuit dans le quartier de Chinatown à New-York, ce qui m’a particulièrement plu.

L’acteur principal, Peter Vack, que j’ai découvert dans ce film m’a convaincu malgré un rôle pas si évident à jouer, tandis Julia Fox, interprétant Scarlett, est tout à fait dans son élément avec ce rôle de dominatrix.

## 🎯 Qui l'apprécierait

Ceux qui aiment les films underground.

## 🔗 Œuvres reliées

[Les Olympiades](/films/les-olympiades) pour son côté camgirl et même pour une partie du scénario.
