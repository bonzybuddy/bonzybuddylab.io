---
title: "Phantom Thread"
date: 2021-04-04T11:56:59+02:00
# maj: "08-02-2021"
draft: false
author: "Paul Thomas Anderson"
principal_actors: "Daniel Day Lewis, Lesley Manville, Vicky Krieps"
genres:
  - Drame
release_year: "2017"
duree: "2h10"
cover:
  img: https://m.media-amazon.com/images/M/MV5BMWJkNzBkM2UtYWFlMC00NmEwLTgxOGUtMjVmMzYyZjgyMmEzXkEyXkFqcGdeQXVyMjM4NTM5NDY@._V1_.jpg
  source: https://www.imdb.com/title/tt5776858/?ref_=nv_sr_srsg_0
---

## 🗣 De quoi ça parle

Un grand couturier recherche l'inspiration à travers une femme qui serait son modèle. Il travaille avec sa sœur et crée de magnifiques robes pour une riche clientèle internationale. Il a une manière de travailler bien à lui, dans le calme absolu et selon des routines bien définies qui ne doivent pas être bousculées. C'est finalement une histoire d'amour entre le couturier et une serveuse d'un restaurant de bord de mer dans la campagne anglaise. Ils finissent par habiter et travailler ensemble malgré de nombreux désaccords sur leurs modes de vie.

## 🔎 Comment je l'ai découvert

Au cinéma, avec le pass Télérama

## 💭 Ce que j'en ai pensé

Film absolument incroyable, un chef d'œuvre que je classe comme mon film préféré de l'année 2018. Je ne peux que vous le recommander.

J'ai adoré la qualité des acteurs, les plans, la lumière, la beauté des costumes, la musique douce du piano et des violons.

## 🎯 Qui l'apprécierait

Ceux qui aiment la couture, les films tranquilles avec une belle musique.

Ceux qui ne recherchent pas spécialement des films à suspense.

## 📽 Films reliés

Les autres films du même réalisateur ou avec le même acteur (_There will be blood_ combine les deux critères). Des films sur les grands couturiers comme Yves Saint Laurent ou Christian Dior.


