---
title: "The French Dispatch"
date: 2021-10-28T11:52:13+02:00
# maj: "08-02-2021"
draft: false
author: "Wes Anderson"
principal_actors: "Timothée Chalamet, Bill Muray, Benicio del Toro"
genres:
  - Comédie Dramatique
release_year: "2021"
duree: "1h43"
cover:
  img: https://m.media-amazon.com/images/M/MV5BNmQxZTNiODYtNzBhYy00MzVlLWJlN2UtNTc4YWZjMDIwMmEzXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg
  source: https://www.imdb.com/title/tt8847712/?ref_=nv_sr_srsg_0
---

_Cette review est extraite de mon [Top 10 des films d'octobre 2021]({{< ref " /films/top10.md#the-french-dispatch" >}})_

Je suis allé voir The French Dispatch uniquement en raison de la bande-annonce, qui m'a happé par ses couleurs, ses plans intrigants et son casting incroyable. D'habitude, j'essaie de ne pas regarder la bande-annonce ou lire le synopsis avant de voir un film. Je me fie uniquement à l'affiche, ce qui est tout aussi idiot mais bon, chacun sa méthode.

Le rédacteur en chef de The French Dispatch vient de décéder. Quatre journalistes racontent alors une histoire qu'ils ont écrites se déroulant à Ennui-sur-Blasé, une petite ville française dont The French Dispatch est le journal local.

Les dialogues et la voix-off censée nous expliquer le contexte vont si vite qu'il est vraiment compliqué de suivre le film. Les plans aussi vont trop vite alors qu'ils sont si riches en détails et mériteraient que la caméra s'y attarde davantage. A cause de cette frénésie, le spectateur est perdu, noyé au milieu d'un nombre considérable d'éléments. Il n'y a aucun personnage principal car les quatre histoires sont complètement indépendantes. Ce scénario alambiqué et cette absence de fil conducteur gâchent considérablement un film qui aurait pu être sublime. L'alternance de noir&blanc/couleur est vraiment bien, on passe d'un format 1,37 à un plan large, bref ça foisonne de bonnes idées. Chaque plan est un tableau, le travail de Wes Anderson est indéniable sur ce point mais la mise en scène devient lassante à la longue tellement elle est étriquée et carrée. La musique aussi est à l'honneur mais elle est trop souvent coupée par l'intervention d'un personnage. Le casting est pourtant incroyable, tous les acteurs du moment sont là : Chalamet, Seydoux, Brody, Wright, Amalric, del Toro, Murray, Khoudri, Waltz, Park, Wilson, Swinton ! La liste est interminable. Malheureusement, malgré ça je n'ai pas réussi à accrocher, et, à l'image du nom de la ville, je me suis ennuyé. De plus, tous les plus beaux éléments du film se retrouvent dans la bande-annonce qui supprime la surprise. Malgré de très bons éléments indéniables, j'ai donc été très déçu par The French Dispatch.

