---
title: "Old Boy"
date: 2021-11-23T15:32:45+01:00
# maj: "08 Fév 2021"
draft: false
author : "Park Chan-wook"
principal_actors: "Choi Min-sik, Yoo Ji-Tae, Kang Hye-jeong"
genres:
  - Action
  - Thriller
  - Drame
release_year: "2003"
duree : "2h"
cover: 
  img: https://m.media-amazon.com/images/M/MV5BMTI3NTQyMzU5M15BMl5BanBnXkFtZTcwMTM2MjgyMQ@@._V1_.jpg
  source: https://www.imdb.com/title/tt0364569/?ref_=nv_sr_srsg_0
#toc: false
---
## 🗣 De quoi ça parle

Le jour de l'anniversaire de sa fille, Oh Dae-Su est capturé et séquestré. Sa prison est une chambre classique où il devient fou car il ne sait absolument pas qui l'a emprisonné ni pourquoi. De plus, il apprend que sa femme a été assassinée, qu'il est le principal suspect et que sa fille de quelques mois est placée en famille d'accueil. 

Quinze ans plus tard, il est relâché, toujours sans raison. Oh Dae-Su n'a qu'une envie, celle de se venger. Il commence alors une furieuse traque dans l'espoir de retrouver son ravisseur... 

{{% spoiler %}}
Il commence par se rendre dans un restaurant où il fait la rencontre d'une jeune serveuse, Mi-do. Il tente de coucher avec elle mais elle commence par refuser malgré son attirance pour lui (il réussira par la suite). 

En goûtant les raviolis d'un restaurant, Oh Dae-Su se remémore celles qu'on lui a servi pendant ses quinze années de captivité. Après avoir filé le livreur du restaurant, il torture sauvagement un homme de main pour obtenir des informations. Il reçoit alors un mystérieux appel de son geôlier qui lui donne cinq jours pour le chercher et le trouver. Si Oh-Dae Su réussit, il se suicidera, sinon c'est Mi-do qui sera tuée. 

Oh Dae-Su parvient enfin à retrouver cet homme démoniaque, Woo-jin Lee, qui s'avère être un ancien camarade de lycée. Ce dernier s'était fait surprendre par Oh Dae-Su en train de violer sa propre sœur. A la suite de cela, une rumeur s'était propagée et sa sœur s'était suicidée. Depuis ce jour, Woo-jin Lee en veut à Oh Dae-Su qui avait trop parlé. Woo-jin Lee a donc minutieusement préparé sa vengeance en l'enfermant pendant quinze ans puis en s'arrangeant pour qu'Oh Dae-Su couche avec sa propre fille (la serveuse Mi-do) qu'il n'a pas pu reconnaître car elle était en bas âge lors de son arrestation. C'est aussi lui qui a commandé l'assassinat de la femme de Oh Dae-Su et répandu la rumeur que ce dernier était l'assassin.

La confrontation finale entre Oh Dae-Su et Woo-jin Lee est terrible car il lui révèle cette vengeance affreuse et machiavélique qui plonge Oh Dae-Su dans une stupeur et une horreur sans nom. Il devient complètement fou et va jusqu'à se couper la langue. Woo-jin Lee respecte alors sa parole et se suicide. Oh Dae-Su est dévasté par les évènements. Dans la dernière scène, il tente de se faire effacer la mémoire par la femme qui l'avait hypnotisé au téléphone dans la scène du poulpe.
{{%/ spoiler %}}

## 🔎 Comment je l'ai découvert

Par hasard avec [Sacha](https://hjkl.it), en cherchant un film à voir le soir même. Old Boy est par ailleurs considéré comme un des meilleurs films coréens.

## 💭 Ce que j'en ai pensé

Le film est assez particulier et très sombre. Sombre dans l'image et dans l'histoire. Si vous lisez le spoiler vous comprendrez pourquoi l'histoire est si hardcore. En sortant de la salle, j'avais un avis mitigé mais quand j'y repense aujourd'hui, le scénario est vraiment bien écrit. L'histoire est incroyable et les acteurs principaux sont formidables, ils nous transmettent leur caractère et leurs émotions si profondes. Les scènes de combat sont superbes notamment celle où Oh Dae-Su s'attaque seul à une quinzaine d'hommes de main. Les effets rajoutés au montage et le positionnement latéral de la caméra est du plus bel effet et me rappelle un peu Matrix. La première scène est incroyable, je vous laisse la découvrir et le dénouement final est époustouflant car il révèle toute l'ampleur du scénario qui pousse l'expression "œil pour œil, dent pour dent" à un extrême.

Comme toujours, j'apprécie d'autant plus les films en VO non anglaise qui apportent un charme aux films. 

Je pense que l'on peut classer Old Boy parmi les films à voir au moins une fois dans sa vie. 

## 🎯 Qui l'apprécierait

Les amateurs de thriller asiatiques. Ceux qui aiment les scènes (de combat) violentes et sanglantes.

## 🔗 Œuvres reliées

Les deux autres films qui constituent la trilogie *Vengeance* de Park Chan-wook, à savoir [Sympathy for Mr Vengeance](https://www.imdb.com/title/tt0310775/?ref_=nv_sr_srsg_0) et [Lady Vengeance](https://www.imdb.com/title/tt0451094/?ref_=nv_sr_srsg_0).

[Les Eternels](https://www.imdb.com/title/tt7298400/?ref_=nv_sr_srsg_0) de Zhangke Jia pour son côté mafia. 

[The Usual Suspects](https://www.imdb.com/title/tt0114814/?ref_=tt_sims_tt_i_2) pour son plot twist magistral.

On pourrait également citer [le comte de Monte-Cristo]({{< ref "../books/monte-cristo/" >}}) où le protagoniste est enfermé en prison pendant de nombreuses années alors qu'il est innocent.
