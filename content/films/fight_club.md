---
title: "Fight Club"
date: 2023-03-25T11:49:33+02:00
# maj: "08 Fév 2021"
draft: false
author: "David Fincher"
principal_actors: "Brad Pitt, Edward Norton, Meat Loaf"
genres:
  - Drame
release_year: "1999"
duree: "2h19"
cover:
  img: https://m.media-amazon.com/images/M/MV5BNDIzNDU0YzEtYzE5Ni00ZjlkLTk5ZjgtNjM3NWE4YzA3Nzk3XkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_.jpg
  source: https://www.imdb.com/title/tt0137523/?ref_=nv_sr_srsg_0
#toc: false
---

Fight Club. Un film devenu culte grâce aux répliques de Brad Pitt sur les règles du Fight Club. Beaucoup d’articles ont déjà été écrit dessus, inutile de paraphraser. Intéressons-nous plutôt à mon ressenti personnel.

Je commencerais en disant que Fight Club est un film qui m’a quelque peu déconcerté. Le montage aide à cet aspect léthargique sous influence psychotrope. A base de ralentis, flashbacks et d’animations dignes d’un jeu vidéo, il plonge le spectateur dans l’univers du narrateur au nom inconnu, un jeune homme insomniaque à la recherche d’un sens dans sa vie. Après avoir tenté de nombreuses thérapies de groupe, toutes plus éloignées de son réel problème les unes que les autres, il rencontre d’abord Marla Singer une toxicomane suicidaire puis Tyler Durden, un vendeur de savons pour le moins excentrique (notez l’humour décalé). La destruction de l’appartement du narrateur, l’amène à vivre avec Tyler dans une maison désaffectée. A la suite d’une bagarre amicale, ils fondent un club de combat — un _fight club_ donc — dans les sous-sols lugubres d’un bar. Leur affaire suscite vite un intérêt grandissant dans une société où l’individu, ici représenté par l’homme masculin, cherche à s’émanciper par la violence contre son semblable. On est là dans le coeur du sujet du livre de Chuck Palahniuk dont est adapté le film éponyme de David Fincher, à savoir une critique du système capitaliste dans toute son ampleur à l’aube du XXIème siècle.

La vie du héros change alors pour se consacrer entièrement à ce club marginal. Vivant à trois dans la maison toujours insalubre avec Maria et Tyler, ses déboires dans la société civilisée s’ensuivent notamment avec son patron. L’activité du fight club, de plus en plus grandissante, provoque la formation d’une milice ultra-violente dont les membres se retrouvent tous dans cette fameuse maison délétère pour l’organisation d’une vaste opération terroriste.

La disparition soudaine de Tyler mène le narrateur sur ses traces lui faisant découvrir une vérité surprenante sur sa santé mentale et sur ses actions antécédentes.

{{% spoiler %}}

Le narrateur découvre que Tyler Durden n’est en réalité qu’une seule et même personne et qu’il est sujet au trouble dissociatif de l’identité. C’est donc lui seul qui a créé le fight club débouchant sur la milice et ses projets terroristes de destruction. De nombreux flashbacks surviennent, la rencontre avec ce fameux vendeur de savon, l’explosion de son appartement, la bagarre initiatrice, les rapports sexuels avec Maria, tout cela n’était donc que l’ouvrage que d’une seule personne : lui-même. Je vous laisse néanmoins découvrir la scène de fin qui est mythique.

{{%/ spoiler %}}

Avec Fight Club on a donc un plot twist à la _The Usual Suspect_ ce qui n’est pas pour me déplaire. Brad Pitt, dans une forme olympique, incarne à merveille son rôle de dépravé drogué, marginal et violent, complètement fou et de mauvaise influence. Le personnage de Marla, en jeune femme hystérique, accentue le caractère décalé et marginal du film.

Je pense que c’est un film que l’on apprécie à revoir même sans la surprise finale pour justement se concentrer sur les détails que l’on ne pouvait remarquer au premier visionnage.

Finissons enfin ce billet sur la musique des Dust Brothers et des Pixies aussi hypnotisantes que l’ambiance du film et qui ne vous laisseront pas indifférent.

Vous l'aurez compris, je vous le recommande.
