---
title: "First Cow"
date: 2021-10-24T15:53:09+02:00
# maj: "08-02-2021"
draft: false
author: "Kelly Reichardt"
principal_actors: "John Magaro, Orion Lee"
genres:
  - Western
  - Drame
release_year: "2019"
duree: "2h"
cover:
  img: https://m.media-amazon.com/images/M/MV5BMGUzMGEzM2UtMDg2Ny00Yjk1LTgxMTctMWI1ZGM0ZDBiYjgxXkEyXkFqcGdeQXVyMTA4NjE0NjEy._V1_.jpg
  source: https://www.imdb.com/title/tt9231040/?ref_=fn_al_tt_1
---

_Cette review est extraite de mon [Top 10 des films d'octobre 2021]({{< ref "/films/top10.md#first-cow" >}})_

First Cow est un western mais qui ne ressemble à aucun autre western. Il n'a du western traditionnel que l'époque et le lieu, autrement dit l'Amérique au XIXe siècle. First Cow est adapté du roman _The Half-Life_ de Jonathan Raymond. J'ai trouvé la bande-annonce peu représentative du film et qu'elle dévoile une bonne partie de l'intrigue. C'est la raison pour laquelle je n'aime pas regarder les bandes-annonces ni lire les synopsis avant de voir un film.

L'histoire suit un cuisinier faisant la rencontre d'un immigré chinois et qui s'associent dans le but de gagner leur vie. Ils trouvent une idée qui leur permet de gagner pas mal d'argent. Cependant, l'élément clé de leur réussite repose sur la vache du grand chef de la colonie. Cette vache est d'ailleurs la première vache importée dans l'état d'Oregon d'où le titre du film. Mais un élément va alors faire basculer leur projet.

La première chose qui frappe est que le film est tourné au format 1,37 c'est-à-dire que l'image est presque carrée et dénote une fois de plus avec les westerns traditionnels aux plans très larges. Le film est lent mais au sens tranquille et reposant. La musique de William Tyler avec ses airs de country est peu présente mais le film n'en pâtit pas. Il n'y a aucune brutalité comme c'est le cas dans un western classique, il n'y pas l'omniprésence des armes et du culte de l'homme viril. Ici, place à la forêt vierge avec nos deux pionniers qui vivent leur vie du mieux qu'ils peuvent dans cette nature qu'est l'Amérique de l'époque. Un film simple en apparence mais poignant grâce à l'empathie des deux protagonistes. Une très belle découverte du cinéma indépendant américain.
