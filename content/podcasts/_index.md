---
title: "Podcast"
---

- [EP18 : On lit des vannes avec Ronan, Sacha et Thomas](https://anchor.fm/ilonehayek/episodes/EP18--On-lit-des-vannes-avec-Ronan--Sacha-et-Thomas-e11flp0/a-a5lo7fk)
- [EP17 : Le 1er filmé ! avec Sacha, Thomas et Ronan](https://anchor.fm/ilonehayek/episodes/EP17--Le-1er-film---avec-Sacha--Thomas-et-Ronan-e11c5jr/a-a5l6d9s)
- [EP16 bonus : Freestyle de Jozehef](https://anchor.fm/ilonehayek/episodes/EP16-bonus--Freestyle-de-Jozehef-e10ulbp/a-a5iv5lm)
- [EP16 : Casino et nos années de collège avec Octave et Joseph](https://anchor.fm/ilonehayek/episodes/EP16--Casino-et-nos-annes-de-collge-avec-Octave-et-Joseph-e10uk8s/a-a5iuvd)
- [EP15 : La planche à voile avec Ronan et Benjamin](https://anchor.fm/ilonehayek/episodes/EP15--La-planche--voile-avec-Ronan-et-Benjamin-e10ktvl/a-a5h99bn)
- [EP14 : Nomade ou sédentaire, nos projets de voyages avec Mathilde](https://anchor.fm/ilonehayek/episodes--EP14--Nomade-ou-sdentaire--nos-projets-de-voyages-avec-Mathilde-evn536/a-a5ce68a)
- [EP13 : Nos années de prépa avec Arthur et Léo](https://anchor.fm/ilonehayek/episodes/EP13--Nos-annes-de-prpa-avec-Arthur-et-Lo-eveh86/a-a5b2ajh)
- [EP12 : On passe un bon moment avec Eloïse et Wilan](https://anchor.fm/ilonehayek/episodes/EP12--On-passe-un-bon-moment-avec-Elose-et-Wilan-euk9vi/a-a56vjcn)
- [EP11 : La photo argentique avec Ronan](https://anchor.fm/ilonehayek/episodes/EP11--La-photo-argentique-avec-Ronan-eubt7j/a-a55p0p5)
- [EP10 : ERASMUS et fitness avec Octave et Joseph](https://anchor.fm/ilonehayek/episodes/EP10--ERASMUS-et-fitness-avec-Octave-et-Joseph-etpiac/a-a53ckc9)
- [EP9 : Web bloat & Rubic avec Sacha](https://anchor.fm/ilonehayek/episodes/EP9--Web-bloat--Rubic-avec-Sacha-esf1rg/a-a4tvp6a)
- [EP8 : Nos études, architecture et textile avec Ronan](https://anchor.fm/ilonehayek/episodes/EP8--Nos-tudes--architecture-et-textile-avec-Ronan-esjlqv/a-a4ugs3r)
- [EP7 : Nos pires anecdotes d'été avec Joseph](https://anchor.fm/ilonehayek/episodes/EP7--Nos-pires-anecdotes-dt-avec-Joseph-ertl9r/a-a4rskjf)
- [EP6 : Le vélo avec Thomas](https://anchor.fm/ilonehayek/episodes/EP6--Le-vlo-avec-Thomas-er9q3m/a-a4pqcra)
- [EP5: Les Jeux Vidéos mainstream avec Bastien, Thomas, Walid et Wilan](https://anchor.fm/ilonehayek/episodes/EP5--Les-Jeux-Vidos-mainstream-avec-Bastien--Thomas--Walid-et-Wilan-eq8k5p/a-a4k01e4")
- [EP4 : La Musique qui nous a marqué avec Mathilde](https://anchor.fm/ilonehayek/episodes/EP4--La-Musique-qui-nous-a-marqu-avec-Mathilde-eq8jvm/a-a4jvv7k)
- [EP3 : Le Rap avec Bastien, Thomas, Walid et Wilan](https://anchor.fm/ilonehayek/episodes/EP3--Le-Rap-avec-Bastien--Thomas--Walid-et-Wilan-eq5gav/a-a4jav1c)
- [EP2 : Voyages, anecdotes et extraterrestres avec Eloïse](https://anchor.fm/ilonehayek/episodes/EP2--Voyages--anecdotes-et-extraterrestres-avec-Elose-eplpvd/a-a4g5d7n)
- [EP1 : L'IA avec Arthur](https://anchor.fm/ilonehayek/episodes/EP1--LIA-avec-Arthur-epgkll/a-a4f4j5t)
