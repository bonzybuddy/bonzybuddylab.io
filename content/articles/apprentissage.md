---
title: "Les meilleures méthodes d'apprentissage"
date: 2021-04-23T18:42:33+02:00
#maj: "08 Fév 2021"
draft: false
genres:
  - Life Advice
  - Productivité
  - Etudes
author: "ilone"
---

Voici la suite de mon précédent article [Comment apprendre n'importe quel nouveau sujet]({{< ref "../articles/comment_apprendre_un_nouveau_sujet/" >}}). Il s'agit surtout d'un approfondissement et vous pouvez très bien lire les deux articles indépendamment. Cet article regroupe donc toutes les méthodes que je trouve pertinentes concernant l'apprentissage et surtout la mémoire. Ces méthodes s'appliquent très bien aux étudiants qui ont besoin d'apprendre et de comprendre un grand nombre de connaissances mais aussi à n'importe quelle personne souhaitant développer un sujet. L'article sera divisé en 3 parties qui sont : Comprendre - Retenir - Se concentrer

## Comprendre

La première étape lorsque l'on découvre un sujet est de bien le comprendre. Il s'agit des fondations à avoir.

Commençons par la méthode dite de Feynman, du nom du physicien Richard P. Feynman. Cette méthode consiste à être capable d'expliquer le sujet à n'importe quelle personne non-experte, comme par exemple un enfant. Choisissez des termes simples et soyez sûrs de décortiquer chaque aspect afin de bien vous faire comprendre. Vous pouvez utiliser des schémas ou tout autre moyen pour assurer la compréhension. Une fois cette étape effectuée, il se peut que certaines zones soient toutefois mal comprises et donc mal expliquées. C'est le signe que vous devez revoir vos notes qui vous ont permis de comprendre le sujet pour la première fois. Relisez-les et remaniez-les pour éliminer chaque faille de vos explications. Préparez-vous aux questions du type "Comment ça fonctionne ?" ou "Pourquoi dans ce sens ?". L'important est de réussir à déceler les points qui vous paraissent acquis mais qui en réalité ne le sont pas. On pense souvent connaître tel ou tel fonctionnement alors que ce n'est pas le cas. Feynman disait "vous ne devez pas vous duper vous-même mais vous êtes la personne la plus facile à duper".

La deuxième méthode dont j'ai déjà parlé dans l'article précédent est de découvrir le sujet à l'aide d'une _mind map_. Explorez chaque domaine du sujet afin de savoir quelles branches vous souhaitez développer. Lorsque vous connaissez ces différentes branches, il vous est plus facile de comprendre et de retenir les liens entre chaque concept. Vous obtenez alors une vue globale sur le sujet.

Passons maintenant à la méthode ultime, celle de l'apprentissage actif. Cette méthode est à mi-chemin entre **comprendre** et **retenir**, elle offre donc une belle transition entre les 2 parties.

Relire ses notes n'est pas vraiment très efficace pour apprendre. Diverses études le démontrent, il existe des méthodes bien plus performantes. Celle de l'apprentissage actif est parfois considérée comme la meilleure. Il faut se tester soi-même, autrement dit vérifier si l'on a bien compris les différents aspects. L'idée à retenir est d'être **actif** dans son apprentissage et dans sa révision. En relisant des notes ou un texte on est au contraire **passif**.

Prenons l'exemple avec un livre. Une fois un chapitre lu, fermez le livre et posez-vous des questions sur ce que vous avez retenu. Etes-vous capables de ressortir les informations à partir de vos propres phrases ? Si oui c'est que vous pouvez passer au chapitre suivant, vous y reviendrez par la suite. Si non, assurez-vous d'avoir bien compris et réessayez.

Au lieu d'écrire des notes que vous relirez inutilement par la suite, vous pouvez plutôt écrire des questions qui résument votre leçon et qui vous forceront à trouver la réponse par vous-même en réfléchissant et donc en permettant à votre cerveau de mieux intégrer la réponse. Ces questions remplacent les notes et s'intègrent dans une démarche d'apprentissage actif.

Un deuxième type de questionnement est d'utiliser les flashcards, qui sont des cartes double-faces sur lesquelles vous écrivez une question sur l'endroit et la réponse sur l'envers. C'est une excellente méthode pour apprendre par cœur et de nombreux étudiants en médecine ou en langues l'utilisent. Vous avez alors le choix de faire vous-même les cartes avec du papier ou bien d'utiliser une application comme Anki qui permet quelque chose d'un peu plus poussé. Avec l'algorithme d'Anki, vous combinez les deux méthodes d'apprentissage actif et de répétition espacée que nous allons voir dans la prochaine partie : Retenir.

## Retenir

Après avoir compris le sujet il faut être capable de le retenir pour pouvoir le réutiliser à tout moment. Voici 2 grands principes pour retenir au mieux.

La répétition espacée est le fait de raviver sa mémoire sur un sujet de manière périodique mais en laissant un laps de temps de plus en plus long entre chaque séance. L'algorithme d'Anki est configurable selon plusieurs paramètres et vous trouverez des vidéos vous expliquant comment et pourquoi les modifier. Vous pouvez aussi laisser les paramètres de bases qui fonctionnent très bien aussi.

Si vous n'utilisez pas Anki, il existe aussi des manières de pratiquer la répétition espacée. Vous pouvez par exemple utiliser le principe d'emploi du temps rétrospectif comme le conseille [Ali Abdaal](https://aliabdaal.com). Il consiste à remplir les colonnes d'un tableur Excel avec les noms des matières que vous souhaitez réviser. Sur une ligne tout en haut vous mettez les différentes dates de révision. A chaque fois que vous revoyez une matière, vous coloriez la case d'intersection matière-date selon le déroulement de la séance. Si vous vous êtes bien rappelés de votre cours, remplissez la case en vert, sinon en orange ou en rouge. Cette méthode est plutôt adaptée pour réviser un examen.

Pour un cas de non-examen, vous pouvez plutôt utiliser un agenda rétrospectif. Le principe est similaire au précédent. Vous notez dans votre agenda la date à laquelle vous avez appris un nouveau sujet puis vous notez des rappels 2 jours, 1 semaine, 2 semaines et 1 mois plus tard. Ainsi lorsque ces échéances arrivent, vous n'oubliez pas de réviser votre sujet.

Pour plus d'infos sur cette fameuse répétition espacée cliquez [ici](https://ncase.me/remember/).

L'entrelacement consiste à mixer les matières que vous révisez au sein d'une même séance. Autrement dit, au lieu d'avoir de gros blocs d'un même sujet les uns à la suite des autres, faites des blocs plus courts qui constituent un cycle que vous répétez plusieurs fois. L'idée derrière cette méthode est de ne pas rester bloqué sur un sujet et de procéder en spirale, c'est-à-dire de revenir dessus au fur et à mesure.

Une tout autre méthode mais néanmoins très classique est celle de la mnémotechnie c'est-à-dire de trouver des astuces, souvent propres à vous, pour vous rappeler de mots ou concepts. On utilise souvent les premières lettres des mots ou encore des consonnances. Cette méthode reste néanmoins assez limitée pour un sujet complexe et long.

En complément à la mnémotechnie, voici la méthode du _Peg System_. Le cerveau humain est assez peu doué pour se remémorer des choses abstraites comme des nombres ou des mots. A l'inverse, il est extrêmement fort pour se remémorer des concepts concrets comme des objets ou des formes. L'idée du Peg System est d'associer des concepts concrets à des concepts abstraits afin de s'en souvenir plus facilement.

Une autre méthode est celle du palais mental, bien connue des champions de la mémoire. En clair, vous vous construisez mentalement une maison avec différentes pièces dans lesquelles vous vous promenez et y intégrez les différentes choses à retenir. Vous pouvez donc assimiler des liens logiques entre différents concepts. Cette technique fonctionne particulièrement bien avec des objets.

## Se concentrer

Pour apprendre et retenir, il est important de se concentrer afin de maximiser l'assimilation des connaissances. Voici plusieurs méthodes pour augmenter votre concentration.

La technique originale du _Pomodoro_ inventée par l'italien Francesco Cirillo, consiste à diviser votre temps en 25mn de concentration pour 5mn de pause. Après avoir effectué 4 sessions de 25mn, prenez 30mn de pause. Répétez le cycle autant que besoin. Vous pouvez évidemment adapter les temps indiqués mais essayez de garder les proportions entre temps de travail et temps de pause. Si certaines personnes affectionnent tout particulièrement cette technique, d'autres ne s'y font pas, la seule manière de savoir pour vous est de tester !

Pour travailler efficacement, apprenez à réduire les distractions qui viennent vous perturber. Une des choses les plus simples est de mettre son téléphone en mode avion dans une autre pièce ou dans un tiroir pour être concentré le temps nécessaire. Vous retrouverez de nombreux autres conseils dans [cet article]({{< ref "../books/make_time/" >}})

Enfin, la dernière astuce de concentration est d'écouter de la musique en travaillant. Si certaines études démontrent qu'il est contre-productif d'écouter de la musique en travaillant, d'autres nuancent en précisant que la musique sans paroles ne perturbe pas. Attention donc au type de musique. Cela dépendra donc notamment de vos préférences, mais si vous arrivez à vous concentrer et à produire un travail de qualité en écoutant de la musique, continuez !

J'espère que vous aurez appris quelque chose en lisant cet article qui, je le rappelle, se complète avec celui-ci, pour que vous puissiez apprendre et retenir n'importe quel sujet !


