---
title: "Apprendre n'importe quel nouveau sujet"
date: 2021-02-12T12:24:58+01:00
# maj: "08-02-2021"
draft: false
genres:
- Life Advice
- Productivité
- Créativité
author: "ilone"
---

## Pourquoi

Il est souvent difficile de commencer à apprendre un nouveau sujet à partir de rien. Lorsque l'on ne sait pas par où commencer, on abandonne rapidement une idée qui aurait pu nous faire découvrir de nombreuses choses intéressantes. En revanche, si l'on sait où et comment trouver les bonnes infos, il est beaucoup plus facile de s'y mettre et de persévérer.

## But

Chercher et apprendre tous les sujets que vous voulez. Avec cette méthode, vous serez capables de constituer de bonnes bases pour chaque nouveau sujet. Vous serez aussi plus enclin à multiplier le nombre de nouvelles choses que vous aimeriez connaître car vous appliquerez à chaque fois la même méthode.

## Combien de temps

Les bases sont généralement acquises entre 3 jours et 1 semaine de recherche par sujet.

## Comment faire

Avoir des bases dans chaque sujet permet d'obtenir des fondations sur lesquelles s'appuyer par la suite. Ces fondations pourront ensuite se développer au fur et à mesure.

Voici les étapes que je considère importantes dans ce processus. Libre à chacun d'ajouter ou d'en supprimer certaines qui ne paraissent pas nécessaires.

### Découvrir le sujet en construisant une mind map globale

Wikipédia est généralement une bonne première approche pour élaborer une première mind map. Celle-ci sera complétée par la suite et comportera tous les différents domaines du sujet. Il sera alors plus simple de choisir quel(s) domaine(s) en particulier seront à approfondir.

### Trouvez de bons sites internet spécialisés ainsi que des blogs

Les blogs forment une communauté de gens passionnés par un sujet et constituent une aide précieuse pour les débutants. On y retrouve des questions types posées par une majorité de débutants et rien ne vous empêche de poser votre propre question ! Les sites internet spécialisés quant à eux n'ont pas cet avantage d'interactions mais peuvent proposer des guides et des dossiers explicatifs qui s'avèrent très utiles.

### Trouvez des livres traitant du sujet

Le support papier est encore très intéressant d'un point de vue apprentissage. Certains sujets ne sont d'ailleurs approfondis que dans des livres. Cependant il s'agit parfois d'ouvrages trop "poussés" qui ne sont pas forcément adaptés aux débutants. Hormis cela, un livre est généralement un très bon complément aux blogs et guides en ligne. De bons livres peuvent parfois être recommandés sur les blogs.

### Trouvez des vidéos en rapport

YouTube est une mine d'or qui regorge de tutos et d'explications de passionnés. De plus, le format vidéo offre une réelle plus-value dans l'explication de sujets. Abonnez-vous aux chaînes qui vous paraissent les plus pertinentes ou qui vous ont été recommandées. Il existe d'autres plateformes vidéos que YouTube comme Vimeo mais aucune n'est plus fournie.

### Trouvez des logiciels adéquats

Ce point n'est pas forcément adapté à tous les sujets. Néanmoins si c'est le cas, il convient de choisir les bons outils qui vous permettront de progresser plus vite et d'aimer les utiliser. Encore une fois, les meilleurs logiciels seront souvent conseillés sur les blogs ou dans des vidéos. Personnellement je privilégie des logiciels open-source gratuits qui peuvent être tout aussi performants et complets que leurs équivalents propriétaires.

### Trouvez des articles de recherche

Ce point dépend également du sujet. Tous ne bénéficient pas de recherches scientifiques mais si c'est le cas, les articles de recherche sont une très bonne source. Car ils constituent l'information brute et non transformée par des intermédiaires qui pourraient l'avoir mal interprétée. Les points négatifs résident dans le fait qu'ils sont rédigés en anglais et qu'ils ne sont pas toujours accessibles gratuitement. Si vous êtes étudiant rapprochez-vous de votre Bibliothèque Universitaire (BU), ils auront sûrement des accès privilégiés pour les revues scientifiques. Sinon, des sites comme ResearchGate regroupent de nombreux articles gratuits.

### Trouvez des experts du sujet (amis, professeurs, parents...)

Avoir la chance de connaître une personne qui puisse répondre directement à ses questions est un des meilleurs éléments que vous puissiez avoir. Faites cependant attention au fait que cette personne soit capable d'expliquer à un débutant ; les meilleurs experts ne sont pas forcément les meilleurs enseignants ! Ce mentor devrait aussi être apte à vous donner d'autres ressources afin de vous documenter par vous-mêmes.

### Approfondissez les sous-sujets que vous avez défini dans votre mind map

Une fois que vous avez effectué toutes ou parties de ces étapes, vous pouvez passer à un autre sujet. Lorsque vous reviendrez sur le premier, les choses auront probablement évoluées de sorte que vous pourrez apprendre de nouvelles choses et mieux comprendre des concepts un peu flous à première vue. Le cerveau a besoin de temps et de repos pour bien assimiler les connaissances (retrouvez [ici]({{< ref "../articles/apprentissage/" >}}) mon article sur la meilleure manière de retenir). S'il s'agit d'un sujet en constante évolution, le temps permettra aussi de faire de nouvelles découvertes qui n'auraient pas pu vous être données avant.

## Conclusion

Les sujets sont aujourd'hui si vastes et denses qu'il peut sembler intimidant ou impossible à aborder pour un débutant. Obtenir de bonnes bases nécessite quelques étapes essentielles comme nous l'avons vu. Un autre point important est que la plupart des (bonnes) ressources seront rédigées en anglais, ce qui peut être une barrière pour certain. Voyez plutôt cela comme une opportunité de progresser dans cette langue et d'acquérir un vocabulaire spécifique qui pourra vous être utile.

## Exemple

Pour illustrer mes propos, voici un exemple de la méthode. *Les ressources données ne sont bien évidemment pas exhaustives et sont subjectives.*

**Sujet** : La Photographie (argentique et numérique)

**Blogs/Sites internet (ici plutôt argentiques) :** [https://www.la-photo-argentique.com/](https://www.la-photo-argentique.com/) et  [https://www.collection-appareils.fr/](https://www.collection-appareils.fr/)

**Livres :** le mode d'emploi de votre appareil ! (gratuit et indispensable) sinon *The Photographer's Eye* de Michael Freeman et *The Beginner’s Photography Guide, 2nd Edition* de Chris Gatcum

**Vidéos/Chaînes YouTube :** [https://www.youtube.com/user/adamsbuster](https://www.youtube.com/user/adamsbuster) et [https://www.youtube.com/channel/UC47xYjkC_63TRI6KPpSEO9A](https://www.youtube.com/channel/UC47xYjkC_63TRI6KPpSEO9A)

**Logiciels (ici retouche photos numériques) :** GIMP, [Paint.net](http://paint.net) ou Photoshop

**Articles scientifiques : N/A**


