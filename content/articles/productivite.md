---
title: "Tous mes conseils sur la productivité"
date: 2021-03-07T19:35:11+01:00
# maj: "08-02-2021"
draft: false
genres:
  - Productivité
  - Life Advice
author: "ilone"
---

Tout le monde rêve d'être plus productif, d'accomplir plus de choses et de mener à terme un projet. Malheureusement ce n'est pas toujours aussi simple et il arrive souvent que l'on abandonne.

Je propose de vous donner quelques techniques pour améliorer votre productivité personnelle et vous permettre de réaliser plus de choses. Je n'ai absolument pas inventé ce qui suit et vous avez probablement déjà entendu parler de certaines choses. Mon but ici est de partager un panel de conseils que je trouve pertinents et qui proviennent de ce que j'ai pu apprendre en lisant et en surfant sur Internet.

J'attire votre attention sur le fait que rendre un travail amusant permet de le réaliser bien plus facilement et plus rapidement. Même si ce n'est pas toujours possible de trouver en quoi cela est plaisant, posez-vous la question "Comment faire pour que ce travail soit amusant et me plaise ?" S'il s'agit de quelque chose d'abstrait comme dans des études académiques, essayez de faire correspondre une action concrète. Ce n'est malheureusement pas toujours le cas.

## Commençons par se débarrasser de quelques mythes

- _"Je n'ai pas le temps"_

- _"Je n'ai pas la motivation"_

- _"Si je pouvais faire plusieurs choses à la fois je serais plus efficace"_

_"Je n'ai pas le temps"_

Ne pas avoir le temps de faire telle ou telle chose est généralement faux. En effet, il vaudrait mieux dire "Je consacre mon temps à faire autre chose". Il s'agit donc simplement d'une distribution du temps disponible et une revalorisation des priorités. Une journée dure 24h pour tout le monde. Si certains sont plus productifs c'est simplement dû au fait qu'ils passent moins de temps sur des choses qui apportent peu, notamment les réseaux sociaux et autres gouffres temporels.

_"Je n'ai pas la motivation"_

Ne pas avoir de motivation ne signifie pas grand-chose. La motivation est un leurre que nous avons créé pour nous permettre de ne pas faire une tâche. Oubliez la motivation, concentrez-vous sur la discipline. En effet, c'est avec cette dernière que vous serez réellement plus efficaces (voir lien plus bas : "Screw motivation, what you need is discipline").

_"Si je pouvais faire plusieurs choses à la fois je serais plus efficace"_

Il n'est tout simplement pas possible d'être multitâche. Nous ne sommes pas des machines. Il a été prouvé de nombreuses fois que le cerveau humain ne peut s'acquitter que d'une seule tâche à la fois. Au lieu de vous éparpillez sur plusieurs choses, concentrez-vous sur une tâche principale, qui occupe toute votre attention et votre énergie.

## Je vous propose maintenant de découvrir 2 principes

**Le principe de Parkinson** stipule que le travail remplit automatiquement tout le temps qu'on lui donne. Pensez aux gaz. Ils agissent de la même manière en occupant tout l'espace disponible. J'apprécie beaucoup ce principe car il se manifeste à chaque fois que l'on doit rendre un travail. Pour éviter de vous retrouver piégés par ce principe, fixez-vous des _deadlines_ artificielles. Donnez-vous moins de temps que prévu pour le faire. Si vous avez un dossier à rendre pour dans deux semaines, arrangez-vous pour le finir en une seule.

**Le 2ème principe est de celui de Pareto** ou plus communément appelé 80/20. 80% de nos accomplissements proviennent de seulement 20% de nos efforts. Autrement dit, la majeure partie de ce que l'on produit est essentiellement le fruit d'une petite partie de ce que l'on a dépensé en temps et en énergie. Tout le reste (donc les 20% restants) n'est que détails et ajustements mais ce sont eux qui demandent le plus de temps.

## Enfin, voici quelques conseils

- Être constant et avoir de bonnes habitudes est le meilleur conseil que je puisse vous donnez (voir ma review sur Atomic Habits, lien plus bas)
- Utiliser ses temps morts de manière intelligente et efficace. La pause de midi est un bon exemple de temps mort. Il se peut que celle-ci soit longue comme dans mon cas (2h). En utilisant une partie de ce temps pour avancer dans mes projets je valorise mon temps. En combinant ce conseil avec la règle des 5 minutes (voir plus bas), je me retrouve à écrire une partie de cet article durant ma pause déjeuner. Je me rends compte que cela fait maintenant plus de 40mn que je rédige.
- Supprimer les sources de distraction ! Ceci est un point important de votre _workflow._ En vous focalisant sur une seule tâche, vous êtes plus performants. Ne checkez pas vos messages toutes les 5 minutes, n'allez pas sur YouTube pendant une session de travail.
- Essayer la technique _Pomodoro._ 25 mn de travail suivi de 5 minutes de pauses à répéter 4 fois. Prendre une pause de 15-20mn entre chaque cycle. Les durées sont modulables en fonction de chacun, une session de travail peut varier entre 15 et 45mn et la pause entre 5 et 15mn. Cette méthode ne vous conviendra pas forcément mais je vous encourage à la tester.
- Ne pas baisser les bras si vous ne respectez pas vos engagements à la lettre, acceptez de légères dérives mais ne les laissez pas s'aggraver.
- Il faut aussi savoir prendre du repos de temps en temps. Encore une fois nous ne sommes pas des machines. Programmer un jour complet de repos permet de se libérer l'esprit et de penser à autre chose. Savoir prendre une pause et éviter le surmenage est une qualité indispensable pour quiconque souhaitant achever un gros projet. Il est impossible de tout faire en seule fois. D'ailleurs les idées peuvent parfois surgir lors d'autres activités qui n'ont aucun rapport avec votre travail.

## # La matrice de décision d'Eisenhower

- The Power Hour

Cette méthode consiste à s'occuper des tâches "importantes mais pas urgentes" pendant une heure le matin, avant de vérifier ses messages ou autre. On se crée alors du temps (en réalité il s'agit plutôt d'une redistribution) pour s'occuper de tâches que l'on n'aurait pas fait autrement.

## # Eviter la procrastination

- La règle des 2 minutes

Si une tâche prend moins de 2mn à effectuer, la faire tout de suite, sinon l'écrire afin de la programmer.

- La règle des 5 minutes

Ne passer que 5 minutes à faire une tâche plus longue dans l'optique de faire un peu plutôt que pas du tout. Parfois, ces quelques minutes se transforment en heures car on prend finalement plaisir à l'effectuer.

- La règle de "l'activation cérébrale"

Plutôt que de ne rien faire et de s'affaler dans le canapé en rentrant du travail, lire un article ou mettre sa tenue de sport permet d'activer son cerveau et de le préparer à en faire plus. En changeant de tenue on s'oblige à entamer une activité.

- _Environment Design_

Le principe d'organiser un espace de manière à rendre facile voire évident un travail est un véritable boost. La manière la plus commune de procéder est d'aménager son bureau. Par exemple, si mon bureau est jonché de papiers dans tous les sens, je ne sais pas où est mon stylo et où j'ai rangé mes dernières notes, je n'aurai rien envie de faire. En revanche, si tout est clair sur mon bureau mais aussi dans ma tête, je sais ce que je dois faire, où et comment les trouver alors il est facile de commencer. Retirez tous les freins possibles qui vous empêchent de faire votre travail.

Voici la méthode CCORE de David Allen tirée de son livre Getting Things Done dont vous retrouvez l'intégralité de ma review [ici]({{< ref "../books/getting_things_done/" >}}).

**Capture**

- Ne pas utiliser son cerveau comme une to-do liste, la mémoire n'est pas faite pour ce genre de tâche

- Écrire les choses permet de libérer son esprit et éviter de l'encombrer avec des choses que l'on risque d'oublier. Cela permet aussi de libérer des neurones qui serviront dans l'accomplissement d'autres tâches (notion de bande-passante)

**Clarify**

- Souvent, les étapes suivantes ne sont pas clairement définies, ce qui ne permet pas d'avancer correctement

- Noter quelle est l'action utile immédiate qui permet de faire avancer le projet

**Organize**

- Faire une to-do liste regroupée en catégories

- Avoir une liste d'actions en attentes, une liste Someday/Maybe et un calendrier à jour

**Reflect**

- Relire périodiquement son système afin de s'assurer que vous atteignez les résultats que vous vous étiez fixés et pour vérifier que vous ne faites pas fausse route.

**Engage**

- Accomplir les actions clarifiées à partir des groupes auxquelles elles appartiennent.

## # Pour en savoir plus :

[Atomic Habits]({{< ref "../books/atomic_habits/" >}})

[Screw motivation what you need is discipline](https://www.wisdomination.com/screw-motivation-what-you-need-is-discipline/)

[Getting Things Done]({{< ref "../books/getting_things_done/" >}})

[Ali Abdaal](https://www.aliabdaal.com)

[Digital Productivity Coach](https://digitalproductivity.coach)


