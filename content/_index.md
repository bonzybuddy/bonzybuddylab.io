---
title: Bienvenue sur mon site !
---

Vous y trouverez toutes mes notes concernant les derniers livres et films que j'ai eu l'occasion de lire ou de voir. Ingénieur en textile, je souhaite partager mes idées sur différents sujets. Je suis particulièrement intéressé par le son et l'image, la physiologie du sport, la philosophie et la lecture.

Retrouvez également mon podcast [Agora](https://anchor.fm/ilonehayek) ainsi que ma [chaîne YouTube](https://www.youtube.com/channel/UCyxzFCnQ7XpdCGrxWxQL5yQ)
